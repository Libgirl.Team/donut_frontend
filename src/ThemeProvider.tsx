import React, { ReactNode } from "react";
import {
  createMuiTheme,
  ThemeProvider,
  ThemeOptions
} from "@material-ui/core/styles";

interface Props {
  children: ReactNode;
}

const themeConfig: ThemeOptions = {
  typography: {
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(", ")
  }
};

const theme = createMuiTheme(themeConfig);

export default ({ children }: Props) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);
