import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { gotoAcntsListPage } from "../../actions/workingPage/acntsListPageActions";
import { gotoModelsList } from "../../actions/workingPage/modelsListActions";
import { gotoSingleModel } from "../../actions/workingPage/singleModelActions";
import { handleGotoRegisterModel } from "../../actions/workingPage/registerModelActions";
import { handleLogout } from "../../actions/authPageActions";
import { gotoDefinitionPage } from "../../actions/workingPage/definitionActions";

export function userInfoZoneMDTM(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return {
    handleGotoAcntsList: () => {
      dispatch(gotoAcntsListPage(dispatch));
    },
    handleGotoModelsList: () => {
      dispatch(gotoModelsList(dispatch));
    },
    handleGotoSingleModel: () => {
      dispatch(gotoSingleModel());
    },
    handleGotoRegisterModel: () => {
      dispatch(handleGotoRegisterModel(dispatch));
      //dispatch(gotoRegisterModel());l
    },
    handleLogout: () => {
      dispatch(handleLogout(dispatch));
    },
    handleGotoDefinition: () => {
      dispatch(gotoDefinitionPage(dispatch));
    }
  };
}
