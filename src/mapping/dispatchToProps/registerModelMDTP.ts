import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  setAuthorField,
  setAddressField,
  setModelDescriptionField,
  SET_MAJOR
} from "../../actions/modelAction";
import {
  RegisterModelMatchState,
  REGISTER_MODEL_PAGE
} from "../../definitions/model";
import { filteredAct, setField, setSingle } from "../../actions/utils";
import {
  ModelTypeDict,
  AlgoDict,
  DataTypeDict,
  UsageDict,
  OwnerID
} from "../../solution/modelManage";
import { handleSubmitRegisterModel } from "../../actions/workingPage/registerModelActions";
import {
  SET_ALGO_DESCRIPTION,
  handleChangeAlgoName
} from "../../actions/algoActions";
import {
  SET_MODEL_TYPE_DESCRIPTION,
  handleChangeModelTypeName
} from "../../actions/modelTypeActions";
import { usageZoneMDTP } from "./registerModel/usageZoneMDTP";
import { ModelRegisteringState } from "../../reducers/modelManagementReducer";
import { summaryZoneMDTP } from "./registerModel/summaryZoneMDTP";
import { sourceZoneMDTP } from "./registerModel/sourceZoneMDTP";

export function registerModelPageMDTP(
  dispatch: ThunkDispatch<{}, {}, AnyAction>
) {
  return {
    handleChangeAurhorField: (authorField: string) => {
      dispatch(filteredAct(REGISTER_MODEL_PAGE, setAuthorField(authorField)));
    },
    handleChangeAddressField: (addressField: string) => {
      dispatch(filteredAct(REGISTER_MODEL_PAGE, setAddressField(addressField)));
    },
    handleChangeModelDescription: (description: string) => {
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, setModelDescriptionField(description))
      );
    },

    handleChangeModelTypeName: (
      field: string,
      modelDict: ModelTypeDict,
      editingAlgo: string,
      algoDict: AlgoDict,
      editingUsage: string,
      usageDict: UsageDict,
      editingInput: string,
      editingOutput: string,
      dataTypeDict: DataTypeDict
    ) => {
      dispatch(
        handleChangeModelTypeName(
          dispatch,
          REGISTER_MODEL_PAGE,
          field,
          modelDict,
          editingAlgo,
          algoDict,
          editingUsage,
          usageDict,
          editingInput,
          editingOutput,
          dataTypeDict
        )
      );
    },
    handleChangeModelTypeDescription: (field: string, editable: boolean) => {
      if (editable) {
        dispatch(
          filteredAct(
            REGISTER_MODEL_PAGE,
            setField(SET_MODEL_TYPE_DESCRIPTION, field)
          )
        );
      }
    },
    handleChangeAlgoName: (
      field: string,
      editable: boolean,
      algoDict: AlgoDict
    ) => {
      if (editable) {
        dispatch(
          handleChangeAlgoName(dispatch, REGISTER_MODEL_PAGE, field, algoDict)
        );
      }
    },
    handleChangeAlgoDescription: (field: string, editable: boolean) => {
      if (editable) {
        dispatch(
          filteredAct(
            REGISTER_MODEL_PAGE,
            setField(SET_ALGO_DESCRIPTION, field)
          )
        );
      }
    },

    handleSelectVersion: (major: number) => {
      dispatch(filteredAct(REGISTER_MODEL_PAGE, setSingle(SET_MAJOR, major)));
    },

    handleSubmitRegisterModel: (
      regState: ModelRegisteringState,
      match: RegisterModelMatchState,
      usrID: OwnerID
    ) => {
      dispatch(handleSubmitRegisterModel(dispatch, regState, match, usrID));
    },

    usage: usageZoneMDTP(dispatch),
    summary: summaryZoneMDTP(dispatch),
    source: sourceZoneMDTP(dispatch)
  };
}
