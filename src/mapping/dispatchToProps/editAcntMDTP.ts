import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  handleSubmitEditAcnt,
  setTMChoice,
  setMMChoice,
  setDMChoice,
  handleDeleteAcnt
} from "../../actions/workingPage/editAcntActions";
import { gotoAcntsListPage } from "../../actions/workingPage/acntsListPageActions";
import { MMLv, DMLv, PubLv } from "../../solution/teamManage";

export function editAcntPageMDTP(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return {
    handleDeleteAcnt: (target: string) => {
      dispatch(handleDeleteAcnt(dispatch, target));
    },
    handleCancel: () => {
      dispatch(gotoAcntsListPage(dispatch));
    },
    handleSubmitEditAcnt: (
      target: string,
      tmLv: PubLv,
      mmLv: MMLv,
      dmLv: DMLv
    ) => {
      dispatch(handleSubmitEditAcnt(dispatch, target, tmLv, mmLv, dmLv));
    },
    handleSelectTM: (tmChoice: PubLv, mmChoice: MMLv, dmChoice: DMLv) => {
      /* console.log('before dispatch handleSelevtTM.');
       * console.log(event.target);*/
      dispatch(setTMChoice(tmChoice, mmChoice, dmChoice));
    },
    handleSelectMM: (mmChoice: MMLv) => {
      dispatch(setMMChoice(mmChoice));
    },
    handleSelectDM: (dmChoice: DMLv) => {
      dispatch(setDMChoice(dmChoice));
    }

    // handleSelectTM: (event: React.ChangeEvent<{ value: unknown }>, mmChoice: MMLv, dmChoice: DMLv) => {
    //     /* console.log('before dispatch handleSelevtTM.');
    //      * console.log(event.target);*/
    //     dispatch(setTMChoice(event.target.value as PubLv, mmChoice, dmChoice));
    // },
    // handleSelectMM: (event: React.ChangeEvent<{ value: unknown }>) => {
    //     dispatch(setMMChoice(event.target.value as MMLv));
    // },
    // handleSelectDM: (event: React.ChangeEvent<{ value: unknown }>) => {
    //     dispatch(setDMChoice(event.target.value as DMLv));
    // },
  };
}
