import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  REGISTER_MODEL_PAGE,
  MODEL_REGISTERING_INPUT,
  MODEL_REGISTERING_OUTPUT
} from "../../../definitions/model";
import { UsageDict, DataTypeDict } from "../../../solution/modelManage";
import { filteredAct, setField } from "../../../actions/utils";
import {
  SET_USAGE_DESCRIPTION,
  handleChangeUsageName
} from "../../../actions/usageActions";
import {
  SET_DATA_TYPE_DESCRIPTION,
  handleChangeRgstMdInputName,
  handleChangeRgstMdOutputName
} from "../../../actions/dataTypeActions";

export function usageZoneMDTP(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return {
    handleChangeUsageName: (
      field: string,
      editable: boolean,
      usageDict: UsageDict,
      editingInput: string,
      editingOutput: string,
      dataTypeDict: DataTypeDict
    ) => {
      if (editable) {
        dispatch(
          handleChangeUsageName(
            dispatch,
            REGISTER_MODEL_PAGE,
            field,
            usageDict,
            editingInput,
            editingOutput,
            dataTypeDict
          )
        );
      }
    },
    handleChangeUsageDescription: (field: string, editable: boolean) => {
      if (editable) {
        dispatch(
          filteredAct(
            REGISTER_MODEL_PAGE,
            setField(SET_USAGE_DESCRIPTION, field)
          )
        );
      }
    },

    handleChangeInputName: (
      field: string,
      editable: boolean,
      dataTypeDict: DataTypeDict
    ) => {
      if (editable) {
        dispatch(handleChangeRgstMdInputName(dispatch, field, dataTypeDict));
      }
    },
    handleChangeInputDescription: (field: string, editable: boolean) => {
      if (editable) {
        dispatch(
          filteredAct(
            MODEL_REGISTERING_INPUT,
            setField(SET_DATA_TYPE_DESCRIPTION, field)
          )
        );
      }
    },

    handleChangeOutputName: (
      field: string,
      editable: boolean,
      dataTypeDict: DataTypeDict
    ) => {
      if (editable) {
        dispatch(handleChangeRgstMdOutputName(dispatch, field, dataTypeDict));
      }
    },
    handleChangeOutputDescription: (field: string, editable: boolean) => {
      if (editable) {
        dispatch(
          filteredAct(
            MODEL_REGISTERING_OUTPUT,
            setField(SET_DATA_TYPE_DESCRIPTION, field)
          )
        );
      }
    }
  };
}
