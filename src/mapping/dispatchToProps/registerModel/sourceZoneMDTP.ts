import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { filteredAct, setSingle } from "../../../actions/utils";
import { REGISTER_MODEL_PAGE } from "../../../definitions/model";
import {
  SET_SOURCE_PARAMETERS,
  handleChangePreModelType,
  SET_SOURCE_MAJOR,
  SET_SOURCE_MINOR
} from "../../../actions/sourceActions";
import {
  SET_DATASET_NAME,
  SET_DATASET_DESCRIPTION,
  SET_DATASET_URL
} from "../../../actions/datsetActions";
import { ModelTypeDict } from "../../../solution/modelManage";

export function sourceZoneMDTP(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return {
    handleChangePreModelType: (name: string, modelTypeDict: ModelTypeDict) => {
      dispatch(
        handleChangePreModelType(
          dispatch,
          REGISTER_MODEL_PAGE,
          name,
          modelTypeDict
        )
      );
    },
    handleChangeParams: (field: string) => {
      dispatch(
        filteredAct(
          REGISTER_MODEL_PAGE,
          setSingle(SET_SOURCE_PARAMETERS, field)
        )
      );
    },
    handleSetDatasetName: (field: string) => {
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, setSingle(SET_DATASET_NAME, field))
      );
    },
    handleSetDatasetDescription: (field: string) => {
      dispatch(
        filteredAct(
          REGISTER_MODEL_PAGE,
          setSingle(SET_DATASET_DESCRIPTION, field)
        )
      );
    },
    handleSetDatasetUrl: (field: string) => {
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, setSingle(SET_DATASET_URL, field))
      );
    },

    handleSelectMajor: (n: number) => {
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, setSingle(SET_SOURCE_MAJOR, n))
      );
    },
    handleSelectMinor: (n: number) => {
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, setSingle(SET_SOURCE_MINOR, n))
      );
    }
  };
}
