import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { filteredAct, setSingle } from "../../../actions/utils";
import {
  REGISTER_MODEL_PAGE,
  EvalRegisteringState,
  EffRegisteringState
} from "../../../definitions/model";
import {
  SET_SHOWING_EVAL,
  SET_SHOWING_EFF,
  addEvalRow,
  addEffRow,
  editEvalName,
  editEvalValue,
  editEffName,
  editEffValue
} from "../../../actions/summaryActions";
import { EvalDict, EffDict } from "../../../solution/modelManage";

export function summaryZoneMDTP(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return {
    switchShowingEval: (original: boolean) => {
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, setSingle(SET_SHOWING_EVAL, !original))
      );
    },
    switchShowingEff: (original: boolean) => {
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, setSingle(SET_SHOWING_EFF, !original))
      );
    },
    addEvalRow: (evalTable: EvalRegisteringState[]) =>
      dispatch(filteredAct(REGISTER_MODEL_PAGE, addEvalRow(evalTable))),
    addEffRow: (effTable: EffRegisteringState[]) =>
      dispatch(filteredAct(REGISTER_MODEL_PAGE, addEffRow(effTable))),
    editEvalName: (
      table: EvalRegisteringState[],
      n: number,
      nameField: string,
      dict: EvalDict
    ) =>
      dispatch(
        filteredAct(
          REGISTER_MODEL_PAGE,
          editEvalName(table, n, nameField, dict)
        )
      ),
    editEvalValue: (
      table: EvalRegisteringState[],
      n: number,
      valueField: string
    ) =>
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, editEvalValue(table, n, valueField))
      ),
    editEffName: (
      table: EffRegisteringState[],
      n: number,
      nameField: string,
      dict: EffDict
    ) =>
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, editEffName(table, n, nameField, dict))
      ),
    editEffValue: (
      table: EffRegisteringState[],
      n: number,
      valueField: string
    ) =>
      dispatch(
        filteredAct(REGISTER_MODEL_PAGE, editEffValue(table, n, valueField))
      )
  };
}
