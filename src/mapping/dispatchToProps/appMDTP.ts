import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { handleGoogleLogin } from "../../actions/authPageActions";
import { Acnt } from "../../solution/teamManage";
import { gotoEditAcnt } from "../../actions/workingPage/editAcntActions";
import { editAcntPageMDTP } from "./editAcntMDTP";
import { userInfoZoneMDTM } from "./userInfoZoneMDTP";
import { registerModelPageMDTP } from "./registerModelMDTP";

export function mapDispatchToProps(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return {
    methods: {
      workingPage: workingPageMDTP(dispatch)
    }
  };
}

export function workingPageMDTP(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return {
    acntsListPageMethods: acntsListPageMDTP(dispatch),
    editAcntPageMethods: editAcntPageMDTP(dispatch),
    userInfoZone: userInfoZoneMDTM(dispatch),
    registerModel: registerModelPageMDTP(dispatch)
  };
}

export function acntsListPageMDTP(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return {
    handleGotoEditAcnt: (acnt: Acnt) => {
      dispatch(gotoEditAcnt(dispatch, acnt));
    }
  };
}
