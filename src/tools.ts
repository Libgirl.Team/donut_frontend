export function assertNever(x: never): never {
  throw new Error("Unexpected object: " + x);
}

export function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export interface OptNone {
  isSome: false;
}

export function optNone(): OptNone {
  return {
    isSome: false
  };
}

export interface OptSome<T> {
  isSome: true;
  v: T;
}

export function optSome<T>(v: T): OptSome<T> {
  return {
    isSome: true,
    v
  };
}

export type Optn<T> = OptSome<T> | OptNone;

interface MatchPtrn<T, U> {
  some: (v: T) => U;
  none: () => U;
}

export function matchOptn<T, U>(optn: Optn<T>, ptrn: MatchPtrn<T, U>): U {
  if (optn.isSome) {
    return ptrn.some(optn.v);
  } else {
    return ptrn.none();
  }
}
