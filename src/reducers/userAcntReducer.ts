import { defaultAcnt, Acnt } from "../solution/teamManage";
import { UserAcntActionsSet, SET_USER_ACNT } from "../actions/userAcntActions";

export type UserAcntState = Acnt;

const initialUserAcntState = defaultAcnt;

export function userAcntReducer(
  state: UserAcntState = initialUserAcntState,
  action: UserAcntActionsSet
): UserAcntState {
  switch (action.type) {
    case SET_USER_ACNT:
      return action.userAcnt;
    default:
      return state;
  }
}
