import { filteredReducer } from ".";
import {
  MODEL_REGISTERING_INPUT,
  MODEL_REGISTERING_OUTPUT,
  DataTypeState,
  initDataTypeState
} from "../definitions/model";
import {
  DataTypeActionsSet,
  SET_DATA_TYPE_NAME,
  SET_DATA_TYPE_DESCRIPTION
} from "../actions/dataTypeActions";

export function dataTypeReducer(
  state: DataTypeState = initDataTypeState,
  action: DataTypeActionsSet
): DataTypeState {
  switch (action.type) {
    case SET_DATA_TYPE_NAME:
      return {
        ...state,
        nameField: action.field
      };
    case SET_DATA_TYPE_DESCRIPTION:
      return {
        ...state,
        descriptionField: action.field
      };
    default:
      return state;
  }
}

export const modelRegisteringInputReducer = filteredReducer(
  MODEL_REGISTERING_INPUT,
  dataTypeReducer
);
export const modelRegisteringOutputReducer = filteredReducer(
  MODEL_REGISTERING_OUTPUT,
  dataTypeReducer
);
