import { AuthActionsSet, SET_AUTH_STATE } from "../actions/authPageActions";

export type AuthState = boolean;

export function authReducer(state: AuthState = false, action: AuthActionsSet) {
  switch (action.type) {
    case SET_AUTH_STATE:
      return action.authState;
    default:
      return state;
  }
}
