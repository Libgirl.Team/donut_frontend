import {
  SourceState,
  initSourceState,
  REGISTER_MODEL_PAGE
} from "../definitions/model";
import { filteredReducer } from "./index";
import {
  SourceActionsSet,
  SET_SOURCE_MODEL_TYPE,
  SET_SOURCE_PARAMETERS,
  SET_SOURCE_MAJOR
} from "../actions/sourceActions";

function sourceReducer(
  state: SourceState = initSourceState,
  action: SourceActionsSet
) {
  switch (action.type) {
    case SET_SOURCE_MODEL_TYPE:
      return {
        ...state,
        modelTypeName: action.v
      };
    case SET_SOURCE_PARAMETERS:
      return {
        ...state,
        parameters: action.v
      };
    default:
      return state;
  }
}

export const modelRegisteringSourceReducer = filteredReducer(
  REGISTER_MODEL_PAGE,
  sourceReducer
);
