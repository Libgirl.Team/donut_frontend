import { combineReducers } from "redux";
import { modelRegisteringModelTypeReducer } from "./modelTypeStateReducer";
import { modelRegisteringAlgoReducer } from "./algoStateReducer";
import { modelRegisteringUsageReducer } from "./usageStateReducer";
import { modelRegisteringModelReducer } from "./modelReducer";
import {
  SET_MODEL_TYPE_DICT,
  ModelMngActionsSet,
  SET_ALGO_DICT,
  SET_USAGE_DICT,
  SET_DATA_TYPE_DICT,
  SET_EVAL_DICT,
  SET_EFF_DICT,
  SET_MODEL_DICT,
  SET_DATASET_DICT
} from "../actions/modelMngActions";
import {
  modelRegisteringInputReducer,
  modelRegisteringOutputReducer
} from "./dataTypeStateReducer";
import { ModelMngDictState, initModelMngDictState } from "../definitions/model";
import { modelRegisterSummaryReducer } from "./summaryReducer";
import { modelRegisteringSourceReducer } from "./sourceReducer";
import { modelRegisteringDatasetReducer } from "./datasetReducer";
import { modelRegisteringMatchReducer } from "./modelMatchReducer";

export function modelMngDictReducer(
  state: ModelMngDictState = initModelMngDictState,
  action: ModelMngActionsSet
): ModelMngDictState {
  switch (action.type) {
    case SET_MODEL_DICT:
      return { ...state, modelDict: action.v };
    case SET_MODEL_TYPE_DICT:
      // console.log('modelMngDictReducer SET_MODEL_TYPE_DICT');
      // console.log(action.modelTypeDict);
      return { ...state, modelTypeDict: action.v };
    case SET_ALGO_DICT:
      return { ...state, algoDict: action.v };
    case SET_USAGE_DICT:
      return { ...state, usageDict: action.v };
    case SET_DATA_TYPE_DICT:
      return { ...state, dataTypeDict: action.v };
    case SET_EVAL_DICT:
      return { ...state, evalDict: action.v };
    case SET_EFF_DICT:
      return { ...state, effDict: action.v };
    case SET_DATASET_DICT:
      return { ...state, datasetDict: action.v };
    default:
      return state;
  }
}

export type ModelRegisteringState = ReturnType<typeof modelRegisteringReducer>;

export const modelRegisteringReducer = combineReducers({
  model: modelRegisteringModelReducer,
  modelType: modelRegisteringModelTypeReducer,
  algo: modelRegisteringAlgoReducer,
  usage: modelRegisteringUsageReducer,
  input: modelRegisteringInputReducer,
  output: modelRegisteringOutputReducer,
  source: modelRegisteringSourceReducer,
  dataset: modelRegisteringDatasetReducer,
  match: modelRegisteringMatchReducer,
  summary: modelRegisterSummaryReducer
});

export const modelMngReducer = combineReducers({
  dict: modelMngDictReducer,
  modelRegistering: modelRegisteringReducer
});
