import {
  REGISTER_MODEL_PAGE,
  RegisterModelMatchState,
  initRegisterModelMatchState,
  ModelIdState
} from "../definitions/model";
import { filteredReducer } from ".";
import {
  SET_MATCH_MODEL_TYPE,
  SET_MATCH_USAGE,
  SET_MATCH_ALGO,
  SET_MATCH_INPUT,
  SET_MATCH_OUTPUT,
  SET_MATCH_SOURCE_MODEL_TYPE,
  SetModelMatchActions
} from "../actions/workingPage/registerModelActions";
import { SET_SOURCE_MAJOR, SET_SOURCE_MINOR } from "../actions/sourceActions";
import { matchOptn, optSome, optNone, Optn } from "../tools";

export const modelRegisteringMatchReducer = filteredReducer(
  REGISTER_MODEL_PAGE,
  modelMatchReducer
);

function modelMatchReducer(
  state: RegisterModelMatchState = initRegisterModelMatchState,
  action: SetModelMatchActions
): RegisterModelMatchState {
  switch (action.type) {
    case SET_MATCH_MODEL_TYPE:
      // console.log('registerModelMatchREducer SET_MATCH_MODEL_TYPE.');
      return {
        ...state,
        modelType: action.v
      };
    case SET_MATCH_USAGE:
      // console.log('SET_MATCH_USAGE');
      return {
        ...state,
        usage: action.v
      };
    case SET_MATCH_ALGO:
      // console.log('SET_MATCH_ALGO');
      return {
        ...state,
        algo: action.v
      };
    case SET_MATCH_INPUT:
      // console.log('SET_MATCH_INPUT');
      return {
        ...state,
        input: action.v
      };
    case SET_MATCH_OUTPUT:
      // console.log('SET_MATCH_OUTPUT');
      return {
        ...state,
        output: action.v
      };
    case SET_MATCH_SOURCE_MODEL_TYPE:
      return {
        ...state,
        sourceModelType: action.v
      };
    case SET_SOURCE_MAJOR:
      return {
        ...state,
        sourceModelType: matchOptn<ModelIdState, Optn<ModelIdState>>(
          state.sourceModelType,
          {
            some: mIdSts => {
              return optSome({
                ...mIdSts,
                majorNum: optSome(action.v)
              });
            },
            none: () => optNone()
          }
        )
      };

    case SET_SOURCE_MINOR:
      return {
        ...state,
        sourceModelType: matchOptn<ModelIdState, Optn<ModelIdState>>(
          state.sourceModelType,
          {
            some: mIdSts => {
              return optSome({
                ...mIdSts,
                minorNum: optSome(action.v)
              });
            },
            none: () => optNone()
          }
        )
      };
    default:
      // console.log('registerModelMatchReducer default state:');
      // console.log(state);
      return state;
  }
}
