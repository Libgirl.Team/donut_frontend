import { Acnt } from "../solution/teamManage";
import { AnyAction } from "redux";

export interface TeamManagementState {
  acntsList: Array<Acnt>;
}

export const initialTeamManagementState: TeamManagementState = {
  acntsList: []
};

export function teamManagementReducer(
  state: TeamManagementState = initialTeamManagementState,
  action: AnyAction
) {
  return state;
}
