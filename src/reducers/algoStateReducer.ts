import { filteredReducer } from ".";
import {
  AlgoState,
  initAlgoState,
  REGISTER_MODEL_PAGE
} from "../definitions/model";
import {
  AlgoActionsSet,
  SET_ALGO_NAME,
  SET_ALGO_DESCRIPTION
} from "../actions/algoActions";

export function algoReducer(
  state: AlgoState = initAlgoState,
  action: AlgoActionsSet
): AlgoState {
  switch (action.type) {
    case SET_ALGO_NAME:
      return {
        ...state,
        nameField: action.field
      };
    case SET_ALGO_DESCRIPTION:
      return {
        ...state,
        descriptionField: action.field
      };
    default:
      return state;
  }
}

export const modelRegisteringAlgoReducer = filteredReducer(
  REGISTER_MODEL_PAGE,
  algoReducer
);
