import { combineReducers, AnyAction } from "redux";
import { userAcntReducer } from "./userAcntReducer";
import workingPageReducer from "./workingPage/workingPageReducer";
import { authReducer } from "./authReducer";
import { teamManagementReducer } from "./teamManagementReducer";
import { modelMngReducer } from "./modelManagementReducer";

export const donutApp = combineReducers({
  auth: authReducer,
  userAcnt: userAcntReducer,
  workingPage: workingPageReducer,
  teamManagement: teamManagementReducer,
  modelManagement: modelMngReducer
});

export type DonutAppState = ReturnType<typeof donutApp>;

export default donutApp;

// should try to accept action: AnyAction | FilteredAction
export function filteredReducer<S, A extends AnyAction>(
  filter: string,
  reducerFn: (state: S | undefined, action: A) => S
): (state: S | undefined, action: A) => S {
  return (state: S | undefined, action: A): S => {
    if (state) {
      if ("filter" in action && action.filter === filter) {
        return reducerFn(state, action);
      } else {
        return state;
      }
    } else {
      return reducerFn(state, action);
    }
  };
}
