import {
  DatasetState,
  initDatasetState,
  REGISTER_MODEL_PAGE
} from "../definitions/model";
import { filteredReducer } from "./index";
import {
  DatasetActionsSet,
  SET_DATASET_NAME,
  SET_DATASET_DESCRIPTION,
  SET_DATASET_URL
} from "../actions/datsetActions";

function datasetReducer(
  state: DatasetState = initDatasetState,
  action: DatasetActionsSet
) {
  switch (action.type) {
    case SET_DATASET_NAME:
      return {
        ...state,
        name: action.v
      };
    case SET_DATASET_DESCRIPTION:
      return {
        ...state,
        description: action.v
      };
    case SET_DATASET_URL:
      return {
        ...state,
        url: action.v
      };
    default:
      return state;
  }
}

export const modelRegisteringDatasetReducer = filteredReducer(
  REGISTER_MODEL_PAGE,
  datasetReducer
);
