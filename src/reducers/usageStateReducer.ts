import {
  UsageRenderState,
  initUsageRenderState,
  REGISTER_MODEL_PAGE
} from "../definitions/model";
import { filteredReducer } from "./index";
import {
  UsageActionsSet,
  SET_USAGE_NAME,
  SET_USAGE_DESCRIPTION
} from "../actions/usageActions";

export function usageReducer(
  state: UsageRenderState = initUsageRenderState,
  action: UsageActionsSet
): UsageRenderState {
  switch (action.type) {
    case SET_USAGE_NAME:
      return {
        ...state,
        nameField: action.field
      };
    case SET_USAGE_DESCRIPTION:
      return {
        ...state,
        descriptionField: action.field
      };
    default:
      return state;
  }
}

export const modelRegisteringUsageReducer = filteredReducer(
  REGISTER_MODEL_PAGE,
  usageReducer
);
