import {
  ModelTypeState,
  initModelTypeState,
  REGISTER_MODEL_PAGE
} from "../definitions/model";
import {
  SET_MODEL_TYPE_NAME,
  ModelTypeActionsSet,
  SET_MODEL_TYPE_DESCRIPTION
} from "../actions/modelTypeActions";
import { filteredReducer } from ".";

export function modelTypeReducer(
  state: ModelTypeState = initModelTypeState,
  action: ModelTypeActionsSet
): ModelTypeState {
  switch (action.type) {
    case SET_MODEL_TYPE_NAME:
      return {
        ...state,
        nameField: action.field
      };
    case SET_MODEL_TYPE_DESCRIPTION:
      return {
        ...state,
        descriptionField: action.field
      };
    default:
      return state;
  }
}

export const modelRegisteringModelTypeReducer = filteredReducer(
  REGISTER_MODEL_PAGE,
  modelTypeReducer
);
