import { filteredReducer } from "./index";
import {
  ModelActionsSet,
  SET_AUTHOR_FIELD_ACTION,
  SET_ADDRESS_FIELD_ACTION,
  SET_MODEL_DESCRIPTION_ACTION,
  SET_MAJOR
} from "../actions/modelAction";
import {
  ModelState,
  initModelState,
  REGISTER_MODEL_PAGE
} from "../definitions/model";

export function modelReducer(
  state: ModelState = initModelState,
  action: ModelActionsSet
): ModelState {
  switch (action.type) {
    case SET_AUTHOR_FIELD_ACTION:
      return {
        ...state,
        authorField: action.authorField
      };
    case SET_ADDRESS_FIELD_ACTION:
      return {
        ...state,
        addressField: action.addressField
      };
    case SET_MODEL_DESCRIPTION_ACTION:
      return {
        ...state,
        descriptionField: action.descriptionField
      };
    case SET_MAJOR:
      return { ...state, majorNum: action.v };
    default:
      return state;
  }
}

// type ModelActionFilter = typeof REGISTER_MODEL_PAGE;

// export interface FilteredModelAction {
//     filter: ModelActionFilter
//     type: string,
// }

export const modelRegisteringModelReducer = filteredReducer(
  REGISTER_MODEL_PAGE,
  modelReducer
);
