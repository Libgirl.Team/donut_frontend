// import { ModelsListActionsSet, GOTO_MODELS_LIST_PAGE } from '../../actions/workingPage/modelsListActions';

export const MODELS_LIST_PAGE = "MODELS_LIST_PAGE";

export interface ModelsListPageState {
  variant: typeof MODELS_LIST_PAGE;
}

// const initialModelsListPageState: ModelsListPageState = { variant: MODELS_LIST_PAGE };

// export function modelsListPageReducer(
//     state: ModelsListPageState = initialModelsListPageState,
//     action: ModelsListActionsSet
// ): ModelsListPageState {
//     switch(action.type) {
//         case GOTO_MODELS_LIST_PAGE:
//             return {
//                 ...state,
//                 variant: MODELS_LIST_PAGE,
//             }
//         default:
//             return state;
//     }
// }
