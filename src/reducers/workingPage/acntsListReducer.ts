import { Acnt } from "../../solution/teamManage";

export const ACNTS_LIST_PAGE = "ACNTS_LIST_PAGE";

export interface AcntsListPageState {
  variant: typeof ACNTS_LIST_PAGE;
  acntsList: Array<Acnt>;
}

export const initialAcntsListPageState: AcntsListPageState = {
  variant: ACNTS_LIST_PAGE,
  acntsList: []
};
