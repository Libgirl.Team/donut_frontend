// import { SingleModelActionsSet, GOTO_SINGLE_MODEL_PAGE } from "../../actions/workingPage/singleModelActions";

export const SINGLE_MODEL_PAGE = "SINGLE_MODEL_PAGE";

export interface SingleModelPageState {
  variant: typeof SINGLE_MODEL_PAGE;
}

// const initialSingleModelPageState: SingleModelPageState = { variant: SINGLE_MODEL_PAGE };

// export function singleModelPageReducer(
//     state: SingleModelPageState = initialSingleModelPageState,
//     action: SingleModelActionsSet
// ): SingleModelPageState {
//     switch(action.type) {
//         case GOTO_SINGLE_MODEL_PAGE:
//             return {
//                 ...state,
//                 variant: SINGLE_MODEL_PAGE,
//             }
//         default:
//             return state;
//     }
// }
