export const DEFINITION_PAGE = "DEFINITION_PAGE";

export interface DefinitionPageState {
  variant: typeof DEFINITION_PAGE;
}
