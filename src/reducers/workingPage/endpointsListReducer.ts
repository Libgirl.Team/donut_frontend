import {
  GOTO_ENDPOINTS_LIST_PAGE,
  EndpointsListActionsSet
} from "../../actions/workingPage/endpointsListActions";

export const ENDPOINTS_LIST_PAGE = "ENDPOINTS_LIST_PAGE";

export interface EndpointsListPageState {
  variant: typeof ENDPOINTS_LIST_PAGE;
}

const initialEndpointsListPageState: EndpointsListPageState = {
  variant: ENDPOINTS_LIST_PAGE
};

export function endpointsListPageReducer(
  state: EndpointsListPageState = initialEndpointsListPageState,
  action: EndpointsListActionsSet
): EndpointsListPageState {
  switch (action.type) {
    case GOTO_ENDPOINTS_LIST_PAGE:
      return {
        ...state,
        variant: ENDPOINTS_LIST_PAGE
      };
    default:
      return state;
  }
}
