import {
  AcntsListPageState,
  initialAcntsListPageState
} from "./acntsListReducer";
import {
  AcntsListActionsSet,
  SET_ACNTS_LIST_PAGE_ACTION
} from "../../actions/workingPage/acntsListPageActions";
import { ModelsListPageState } from "./modelsListReducer";
import { SingleModelPageState } from "./singleModelPageReducer";
import {
  ModelsListActionsSet,
  SET_MODELS_LIST_ACTION
} from "../../actions/workingPage/modelsListActions";
import {
  SingleModelActionsSet,
  GOTO_SINGLE_MODEL_PAGE
} from "../../actions/workingPage/singleModelActions";
import {
  GOTO_REGISTER_MODEL_PAGE,
  GotoRegisterModelPageAction
} from "../../actions/workingPage/registerModelActions";
import { EditAcntPageState, EDIT_ACNT_PAGE } from "./editAcntReducer";
import {
  EditAcntActionsSet,
  SET_EDIT_ACNT_ACTION,
  SET_TM_CHOICE,
  SET_MM_CHOICE,
  SET_DM_CHOICE
} from "../../actions/workingPage/editAcntActions";
import { DefinitionPageState } from "./definitionReducer";
import {
  DefinitionActionsSet,
  SET_DEFINITION_ACTION
} from "../../actions/workingPage/definitionActions";
import { RegisterModelPageState } from "./registerModelPageReducer";

export const WORKING_PAGE = "WORKING_PAGE";

export const SINGLE_MODEL_PAGE = "SINGLE_MODEL_PAGE";

export type WorkingPageState =
  | AcntsListPageState
  | EditAcntPageState
  | ModelsListPageState
  | SingleModelPageState
  | RegisterModelPageState
  | DefinitionPageState;

type WorkingPageActionsSet =
  | AcntsListActionsSet
  | EditAcntActionsSet
  | ModelsListActionsSet
  | SingleModelActionsSet
  | GotoRegisterModelPageAction
  | DefinitionActionsSet;

// export const initialWorkingPageState: WorkingPageState = initialLoginPageState;

export const initialWorkingPageState: WorkingPageState = initialAcntsListPageState;

export function workingPageReducer(
  state: WorkingPageState = initialWorkingPageState,
  action: WorkingPageActionsSet
): WorkingPageState {
  // console.log('workingPageReducer State:');
  // console.log(state);
  if (action.type === SET_MODELS_LIST_ACTION) {
    return action.state;
  } else if (action.type === SET_ACNTS_LIST_PAGE_ACTION) {
    return action.state;
  } else if (action.type === SET_EDIT_ACNT_ACTION) {
    return action.state;
  } else if (action.type === GOTO_SINGLE_MODEL_PAGE) {
    return { variant: action.variant };
  } else if (action.type === GOTO_REGISTER_MODEL_PAGE) {
    return {
      variant: action.variant
    };
  } else if (action.type === SET_DEFINITION_ACTION) {
    return action.state;
  } else if (
    state.variant === EDIT_ACNT_PAGE &&
    action.type === SET_TM_CHOICE
  ) {
    return {
      ...state,
      tmChoice: action.tmChoice,
      mmChoice: action.mmChoice,
      dmChoice: action.dmChoice
    };
  } else if (
    state.variant === EDIT_ACNT_PAGE &&
    action.type === SET_MM_CHOICE
  ) {
    return {
      ...state,
      mmChoice: action.mmChoice
    };
  } else if (
    state.variant === EDIT_ACNT_PAGE &&
    action.type === SET_DM_CHOICE
  ) {
    return {
      ...state,
      dmChoice: action.dmChoice
    };
  } else {
    // console.log('workingPageReducer else:');
    // console.log(state);
    return state;
  }
}

export default workingPageReducer;
