import { REGISTER_MODEL_PAGE } from "../../definitions/model";

export interface RegisterModelPageState {
  variant: typeof REGISTER_MODEL_PAGE;
}
