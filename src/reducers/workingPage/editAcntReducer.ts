import {
  Acnt,
  defaultAcnt,
  MMLv,
  DMLv,
  PubLv
} from "../../solution/teamManage";

export const EDIT_ACNT_PAGE = "EDIT_ACNT_PAGE";

export interface EditAcntPageState {
  variant: typeof EDIT_ACNT_PAGE;
  target: Acnt;
  tmChoice: PubLv;
  mmChoice: MMLv;
  dmChoice: DMLv;
}

export const initialAcntsListPageState: EditAcntPageState = {
  variant: EDIT_ACNT_PAGE,
  target: defaultAcnt,
  tmChoice: 0,
  mmChoice: 0,
  dmChoice: 0
};
