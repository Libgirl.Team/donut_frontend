import { filteredReducer } from "./index";
import {
  REGISTER_MODEL_PAGE,
  SummaryState,
  initSummaryState
} from "../definitions/model";
import {
  SummaryActionsSet,
  SET_SHOWING_EVAL,
  SET_SHOWING_EFF,
  SET_EVAL_TABLE,
  SET_EFF_TABLE
} from "../actions/summaryActions";

export function summaryReducer(
  state: SummaryState = initSummaryState,
  action: SummaryActionsSet
) {
  switch (action.type) {
    case SET_SHOWING_EVAL:
      return {
        ...state,
        isShowingEval: action.v
      };
    case SET_SHOWING_EFF:
      return {
        ...state,
        isShowingEff: action.v
      };
    case SET_EVAL_TABLE:
      return {
        ...state,
        evalTable: action.v
      };
    case SET_EFF_TABLE:
      return {
        ...state,
        effTable: action.v
      };
    default:
      return state;
  }
}

export const modelRegisterSummaryReducer = filteredReducer(
  REGISTER_MODEL_PAGE,
  summaryReducer
);
