import { ResResult, reqAxios, mapRR } from "./client";
import {
  ModelTypeDef,
  ModelTypeKnowledge,
  Versions,
  AlgoDef,
  AlgoKnowledge,
  UsageDef,
  UsageKnowledge,
  DataTypeDef,
  DataTypeKnowledge,
  EvalDef,
  EvalKnowledge,
  EffDef,
  EffKnowledge,
  Model,
  ItemValue,
  ModelKnowledge,
  NewModelTypeKnowledge,
  DatasetKnowledge,
  DatasetDef
} from "../solution/modelManage";
import { matchOptn } from "../tools";

export async function getDefList<D>(
  api: string,
  parser: (res: any) => D
): Promise<ResResult<Array<D>>> {
  let resList: ResResult<Array<any>> = await reqAxios(`v1/${api}/list`, {
    withCredentials: true,
    method: "get"
  });
  let parsed: ResResult<Array<D>> = mapRR(resList, ls =>
    ls.map(def => parser(def))
  );
  // console.log(`getDefList by ${api}: `);
  // console.log(parsed);
  return parsed;
}

export const getModelList = () => getDefList("model", parseToModel);
export const getModelTypeDefList = () =>
  getDefList("dict/modeltype", parseToModelTypeDef);
export const getAlgoDefList = () =>
  getDefList("dict/algorithm", parseToAlgoDef);
export const getUsageDefList = () => getDefList("dict/usage", parseToUsageDef);
export const getDataTypeDefList = () =>
  getDefList("dict/datatype", parseToDataTypeDef);
export const getEvalDefList = () =>
  getDefList("dict/evaluation", parseToEvalDef);
export const getEffDefList = () => getDefList("dict/efficiency", parseToEffDef);
export const getDatasetDeflist = () =>
  getDefList("dict/dataset", parseToDatasetDef);

export function parseToModel(res: any): Model {
  const resKnowledge = res.model_knowledge;
  const resEvals = resKnowledge.summary.evaluations;
  const evals = Object.keys(resEvals).map(hash => {
    return { _id: hash, value: resEvals[hash] };
  });
  const resEffs = resKnowledge.summary.efficiencies;
  const effs = Object.keys(resEffs).map(hash => {
    return { _id: hash, value: resEffs[hash] };
  });
  return {
    _id: res._id,
    knowledge: {
      ownerID: resKnowledge.owner_id,
      authorID: resKnowledge.author_id,
      description: resKnowledge.description,
      address: resKnowledge.address,
      tags: [],
      source: {
        previousModel: resKnowledge.source.prev_model,
        params: resKnowledge.source.parameters,
        dataset: resKnowledge.source.dataset
      },
      intrinsics: {
        modelType: resKnowledge.intrinsics.model_type
      },
      summary: {
        eval: evals,
        eff: effs
      }
    }
  };
}

export function parseToItemValue(resItemValue: any): ItemValue {
  const hash = Object.keys(resItemValue)[0];
  return {
    _id: hash,
    value: resItemValue[hash]
  };
}

export function parseToModelTypeDef(res: any): ModelTypeDef {
  // console.log('parseToModelTypeDef');
  // console.log(res);
  let resV = res.versions;
  let version: Versions = {
    newMajor: resV.new_major,
    majorVersions: Object.keys(resV.major_versions).map(keyMajor => {
      const resMajor = resV.major_versions[keyMajor];
      // console.log('parse major:');
      // console.log(resMajor);
      return {
        num: Number(keyMajor),
        newMinor: resMajor.new_minor,
        minorVersions: Object.keys(resMajor.minor_versions).map(keyMinor => {
          // console.log('parseMinor');
          // console.log(`keyMinor: ${keyMinor}`);
          // console.log(`modelID: ${resMajor.minor_versions[keyMinor]}`);
          return {
            num: Number(keyMinor),
            modelID: resMajor.minor_versions[keyMinor]
          };
        })
      };
    })
  };

  const knowledge: ModelTypeKnowledge = {
    ownerID: res.owner_id,
    name: res.name,
    description: res.description,
    algo: res.algorithm,
    usage: res.usage,
    version
  };

  return {
    _id: res._id,
    knowledge
  };
}

export function parseToAlgoDef(res: any): AlgoDef {
  const knowledge: AlgoKnowledge = {
    ownerID: res.owner_id,
    name: res.name,
    description: res.description
    // tags: res.tags,
  };
  return {
    _id: res._id,
    knowledge
  };
}

export function parseToUsageDef(res: any): UsageDef {
  const knowledge: UsageKnowledge = {
    ownerID: res.owner_id,
    name: res.name,
    description: res.description,
    input: res.input_type,
    output: res.output_type
  };
  return {
    _id: res._id,
    knowledge
  };
}

export function parseToDataTypeDef(res: any): DataTypeDef {
  const knowledge: DataTypeKnowledge = {
    ownerID: res.owner_id,
    name: res.name,
    description: res.description
  };
  return {
    _id: res._id,
    knowledge
  };
}

export function parseToEvalDef(res: any): EvalDef {
  const knowledge: EvalKnowledge = {
    ownerID: res.owner_id,
    name: res.name,
    description: res.description,
    unit: res.unit,
    dataset: res.dataset
  };
  return {
    _id: res._id,
    knowledge
  };
}

export function parseToEffDef(res: any): EffDef {
  const knowledge: EffKnowledge = {
    ownerID: res.owner_id,
    name: res.name,
    description: res.description,
    unit: res.unit
  };
  return {
    _id: res._id,
    knowledge
  };
}

export function parseToDatasetDef(res: any): DatasetDef {
  const knowledge: DatasetKnowledge = {
    owner: res.owner_id,
    name: res.name,
    url: res.url,
    description: res.description
  };
  return {
    _id: res._id,
    knowledge: knowledge
  };
}

export async function postModel(
  majorVer: number,
  mdk: ModelKnowledge
): Promise<ResResult<Model>> {
  const reqBody = {
    major_number: majorVer,
    model_knowledge: {
      owner_id: mdk.ownerID,
      author_id: matchOptn(mdk.authorID, {
        some: id => id,
        none: () => ""
      }),
      description: mdk.description,
      address: mdk.address,
      intrinsics: {
        model_type: mdk.intrinsics.modelType
      },
      source: {
        prev_model: mdk.source.previousModel,
        parameters: mdk.source.params,
        dataset: mdk.source.dataset
      },
      summary: {
        evaluations: mdk.summary.eval.reduce((acc: any, x) => {
          acc[x._id] = x.value;
          return acc;
        }, {}),
        efficiencies: mdk.summary.eff.reduce((acc: any, x) => {
          acc[x._id] = x.value;
          return acc;
        }, {})
      }
    }
  };

  const res = await reqAxios("v1/model", {
    withCredentials: true,
    method: "post",
    data: reqBody
  });

  let parsed: ResResult<Model> = mapRR(res, parseToModel);
  return parsed;
}

export async function postModelType(
  mdtk: NewModelTypeKnowledge
): Promise<ResResult<ModelTypeDef>> {
  const reqBody = {
    owner_id: mdtk.ownerID,
    name: mdtk.name,
    description: mdtk.description,
    algorithm: mdtk.algo,
    usage: mdtk.usage
  };

  const res = await reqAxios("v1/dict/modeltype", {
    withCredentials: true,
    method: "post",
    data: reqBody
  });

  let parsed: ResResult<ModelTypeDef> = mapRR(res, parseToModelTypeDef);
  return parsed;
}

export async function postAlgo(
  algoK: AlgoKnowledge
): Promise<ResResult<AlgoDef>> {
  const reqBody = {
    owner_id: algoK.ownerID,
    name: algoK.name,
    description: algoK.description
  };

  const res = await reqAxios("v1/dict/algorithm", {
    withCredentials: true,
    method: "post",
    data: reqBody
  });

  let parsed: ResResult<AlgoDef> = mapRR(res, parseToAlgoDef);
  return parsed;
}

export async function postUsage(
  usageK: UsageKnowledge
): Promise<ResResult<UsageDef>> {
  const reqBody = {
    owner_id: usageK.ownerID,
    name: usageK.name,
    description: usageK.description,
    input_type: usageK.input,
    output_type: usageK.output
  };

  const res = await reqAxios("v1/dict/usage", {
    withCredentials: true,
    method: "post",
    data: reqBody
  });

  let parsed: ResResult<UsageDef> = mapRR(res, parseToUsageDef);
  return parsed;
}

export async function postDataType(
  dataTypeK: DataTypeKnowledge
): Promise<ResResult<DataTypeDef>> {
  const reqBody = {
    owner_id: dataTypeK.ownerID,
    name: dataTypeK.name,
    description: dataTypeK.description
  };

  const res = await reqAxios("v1/dict/datatype", {
    withCredentials: true,
    method: "post",
    data: reqBody
  });

  let parsed: ResResult<DataTypeDef> = mapRR(res, parseToDataTypeDef);
  return parsed;
}

export async function postEval(
  evalK: EvalKnowledge
): Promise<ResResult<EvalDef>> {
  const reqBody = {
    owner_id: evalK.ownerID,
    name: evalK.name,
    description: evalK.description,
    dataset: evalK.dataset
  };

  const res = await reqAxios("v1/dict/evaluation", {
    withCredentials: true,
    method: "post",
    data: reqBody
  });

  let parsed: ResResult<EvalDef> = mapRR(res, parseToEvalDef);
  return parsed;
}

export async function postEff(effK: EffKnowledge): Promise<ResResult<EffDef>> {
  const reqBody = {
    owner_id: effK.ownerID,
    name: effK.name,
    description: effK.description
  };

  const res = await reqAxios("v1/dict/efficiency", {
    withCredentials: true,
    method: "post",
    data: reqBody
  });

  let parsed: ResResult<EffDef> = mapRR(res, parseToEffDef);
  return parsed;
}

export async function postDataset(
  datasetK: DatasetKnowledge
): Promise<ResResult<DatasetDef>> {
  const reqBody = {
    owner_id: datasetK.owner,
    name: datasetK.name,
    description: datasetK.description,
    url: datasetK.url
  };

  const res = await reqAxios("v1/dict/dataset", {
    withCredentials: true,
    method: "post",
    data: reqBody
  });

  let parsed: ResResult<DatasetDef> = mapRR(res, parseToDatasetDef);
  return parsed;
}
