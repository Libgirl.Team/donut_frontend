import { Acnt, TMLv, MMLv, DMLv } from "../solution/teamManage";
import { ResResult, reqAxios, mapRR } from "./client";

interface ResponseAcnt {
  ID: string;
  name: string;
  TM_Level: number;
  MM_Level: number;
  DM_Level: number;
  is_alive: boolean;
}

export function numToTMLv(num: Number): TMLv {
  switch (num) {
    case 0:
      return 0;
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 3;
    case 4:
      return 4;
    default:
      console.log("Response TM Level != 0~4:");
      console.log(num);
      return 0;
  }
}

export function numToMMLv(num: Number): MMLv {
  switch (num) {
    case 0:
      return 0;
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 3;
    default:
      console.log("Response MM Level != 0~3:");
      console.log(num);
      return 0;
  }
}

export function numToDMLv(num: Number): DMLv {
  switch (num) {
    case 0:
      return 0;
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 3;
    default:
      console.log("Response DM Level != 0~3:");
      console.log(num);
      return 0;
  }
}

export function parseToAcnt(res: ResponseAcnt): Acnt {
  return {
    userID: res.ID,
    userName: res.name,
    tmLevel: numToTMLv(res.TM_Level),
    mmLevel: numToMMLv(res.MM_Level),
    dmLevel: numToDMLv(res.DM_Level),
    isAlive: res.is_alive
  };
}

export async function getUserAcnt(id?: String): Promise<ResResult<Acnt>> {
  // let acnt = defaultAcnt; // for testing without backend
  //    console.log('getUserAcnt triggered.');

  let acnt: ResResult<ResponseAcnt> = await reqAxios("v1/account", {
    withCredentials: true,
    method: "get",
    params: (() => {
      if (id) {
        return {};
      } else {
        return { ID: id };
      }
    })()
  });
  //console.log(acnt);
  return mapRR(acnt, a => parseToAcnt(a));
}

/* const acntsList0 = [
 *     newAcnt('ben@libgirl.com', 'BenYo', 3, 3, 3),
 *     newAcnt('balisun@libgirl.com', 'BaliSun', 1, 1, 0),
 * ] // for testing without backend */

export async function getAcntsList(): Promise<ResResult<Array<Acnt>>> {
  let resList = await reqAxios<Array<ResponseAcnt>>("v1/account/list", {
    withCredentials: true,
    method: "get"
  });
  //console.log(resList);
  return mapRR(resList, r => r.map((acnt: ResponseAcnt) => parseToAcnt(acnt)));
}

export async function putAccountLevel(
  target: string,
  tmLv: TMLv,
  mmLv: MMLv,
  dmLv: DMLv
) {
  console.log("putAccountLevel.");
  await reqAxios<void>("v1/account/level", {
    withCredentials: true,
    method: "put",
    data: {
      ID: target,
      TM_Level: tmLv,
      MM_Level: mmLv,
      DM_Level: dmLv
    }
  });
}

export async function deleteAccount(target: string) {
  console.log("start deleteAccount.");
  await reqAxios<void>("v1/account", {
    withCredentials: true,
    method: "delete",
    params: {
      ID: target
    }
  });
}
