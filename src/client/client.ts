export const OTHER_ERR = "OTHER_ERR";

export type ResResult<T> = "401" | typeof OTHER_ERR | T;

export function mapRR<T, U>(rr: ResResult<T>, fn: (x: T) => U): ResResult<U> {
  if (rr === "401" || rr === OTHER_ERR) {
    return rr;
  } else {
    return fn(rr);
  }
}

export const axios = require("axios").default;
// axios.withCredentials = true; //seems no use.
export interface AxiosRes<T> {
  data: T;
}

const backendUrl = "http://localhost:3000/";

export async function reqAxios<T>(
  postUrl: string,
  config: any
): Promise<ResResult<T>> {
  return await axios({
    ...config,
    url: backendUrl + postUrl
  })
    .then((res: AxiosRes<T>) => {
      // console.log('reqAxios res:');
      // console.log(res);
      return res.data;
    })
    .catch((err: any) => {
      // console.log('reqAxios err:');
      // console.log(err);
      if (err.response.status === 401) {
        return "401";
      } else {
        return OTHER_ERR;
      }
    });
}
