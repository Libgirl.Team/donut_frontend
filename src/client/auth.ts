import { sleep } from "../tools";
import { ResResult, reqAxios } from "./client";

const authUrl =
  "https://accounts.google.com/o/oauth2/v2/auth?client_id=498503301951-svfdm1otcljho86hr3semffgu7fld08j.apps.googleusercontent.com&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Fv1%2Foauth%2Foauth2callback&response_type=code&scope=email+profile";

export async function googleLogin() {
  let loginWindow = window.open(authUrl);

  return new Promise<any>(async (resolve, reject) => {
    if (loginWindow != null) {
      while (!loginWindow.closed) {
        await sleep(100);
      }
      resolve();
    } else {
      reject(new Error("get null loginWindow."));
      return;
    }
  });
}

export async function logout(): Promise<ResResult<void>> {
  let res = await reqAxios<void>("v1/oauth/logout", {
    withCredentials: true,
    method: "post"
  });
  return res;
}
