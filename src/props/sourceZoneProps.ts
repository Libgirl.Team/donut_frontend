import { DonutAppProps } from "../components/donutApp";
import {
  SourceZoneProps,
  VersionSelectPairProps,
  VerChoice
} from "../components/registerModelPage/sourceZone";
import { matchOptn, optNone, optSome, Optn } from "../tools";
import { majorVersions, minorVersions } from "../solution/modelManage";
import { ModelIdState } from "../definitions/model";

export function sourceZoneProps(props: DonutAppProps): SourceZoneProps {
  const sourceState = props.state.modelManagement.modelRegistering.source;
  const sourceMthds = props.methods.workingPage.registerModel.source;
  const datasetState = props.state.modelManagement.modelRegistering.dataset;
  return {
    modelType: sourceState.modelTypeName,
    handleChangeModelType: (name: string) =>
      sourceMthds.handleChangePreModelType(
        name,
        props.state.modelManagement.dict.modelTypeDict
      ),
    parameters: sourceState.parameters,
    handleChangeParameters: sourceMthds.handleChangeParams,
    datasetName: datasetState.name,
    handleChangeDatasetname: sourceMthds.handleSetDatasetName,
    datasetDescription: datasetState.description,
    handleChangeDatasetDescription: sourceMthds.handleSetDatasetDescription,
    datasetUrl: datasetState.url,
    handleChangeDatasetUrl: sourceMthds.handleSetDatasetUrl,

    versionsSelect: matchOptn<ModelIdState, Optn<VersionSelectPairProps>>(
      props.state.modelManagement.modelRegistering.match.sourceModelType,
      {
        some: sts => {
          return optSome({
            major: optSome({
              choice: sts.majorNum,
              options: majorVersions(sts.modelType)
            }),
            minor: matchOptn<number, Optn<VerChoice>>(sts.majorNum, {
              some: mjrNum => {
                // console.log("sourceZoneProps versionsSelect:");
                // console.log(minorVersions(sts.modelType, mjrNum));
                return optSome({
                  choice: sts.minorNum,
                  options: minorVersions(sts.modelType, mjrNum)
                });
              },
              none: () => optNone()
            }),
            handleSelectMajor: sourceMthds.handleSelectMajor,
            handleSelectMinor: sourceMthds.handleSelectMinor
          });
        },
        none: () => optNone()
      }
    )
  };
}
