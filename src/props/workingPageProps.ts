import { DonutAppProps } from "../components/donutApp";
import {
  WorkingPageProps,
  UNHANDLED_WP,
  WorkingPagePropsVariant
} from "../components/workingPage";
import { ACNTS_LIST_PAGE } from "../reducers/workingPage/acntsListReducer";
import { EDIT_ACNT_PAGE } from "../reducers/workingPage/editAcntReducer";
import { MODELS_LIST_PAGE } from "../reducers/workingPage/modelsListReducer";
import { REGISTER_MODEL_PAGE } from "../definitions/model";
import { registerModelPageProps } from "./registerModelPageProps";
import { DEFINITION_PAGE } from "../reducers/workingPage/definitionReducer";
import { SINGLE_MODEL_PAGE } from "../reducers/workingPage/singleModelPageReducer";

export function workingPageProps(props: DonutAppProps): WorkingPageProps {
  const mthds = props.methods;
  const state = props.state;

  function genVariant(): WorkingPagePropsVariant {
    if (state.workingPage.variant === ACNTS_LIST_PAGE) {
      return {
        type: ACNTS_LIST_PAGE,
        userAcnt: state.userAcnt,
        acntsList: state.workingPage.acntsList,
        handleGotoEditAcnt:
          mthds.workingPage.acntsListPageMethods.handleGotoEditAcnt
      };
    } else if (state.workingPage.variant === EDIT_ACNT_PAGE) {
      return {
        type: EDIT_ACNT_PAGE,
        target: state.workingPage.target,
        usr: state.userAcnt,
        tmChoice: state.workingPage.tmChoice,
        mmChoice: state.workingPage.mmChoice,
        dmChoice: state.workingPage.dmChoice,
        handleDeleteAcnt:
          mthds.workingPage.editAcntPageMethods.handleDeleteAcnt,
        handleSelectTM: mthds.workingPage.editAcntPageMethods.handleSelectTM,
        handleSelectMM: mthds.workingPage.editAcntPageMethods.handleSelectMM,
        handleSelectDM: mthds.workingPage.editAcntPageMethods.handleSelectDM,
        handleCancel: mthds.workingPage.editAcntPageMethods.handleCancel,
        handleSubmitEditAcnt:
          mthds.workingPage.editAcntPageMethods.handleSubmitEditAcnt
      };
    } else if (state.workingPage.variant === MODELS_LIST_PAGE) {
      return {
        type: MODELS_LIST_PAGE,
        dict: state.modelManagement.dict
      };
    } else if (state.workingPage.variant === REGISTER_MODEL_PAGE) {
      return registerModelPageProps(props);
    } else if (state.workingPage.variant === DEFINITION_PAGE) {
      return { type: DEFINITION_PAGE };
    } else if (state.workingPage.variant === SINGLE_MODEL_PAGE) {
      return { type: SINGLE_MODEL_PAGE };
    } else {
      return {
        type: UNHANDLED_WP
      };
    }
  }

  return {
    userInfoZone: {
      userAcnt: props.state.userAcnt,
      handleLogout: mthds.workingPage.userInfoZone.handleLogout,
      handleGotoModelsList: mthds.workingPage.userInfoZone.handleGotoModelsList,
      handleGotoRegisterModel:
        mthds.workingPage.userInfoZone.handleGotoRegisterModel,
      handleGotoSingleModel:
        mthds.workingPage.userInfoZone.handleGotoSingleModel,
      handleGotoAcntsList: mthds.workingPage.userInfoZone.handleGotoAcntsList,
      handleGotoDefinition: mthds.workingPage.userInfoZone.handleGotoDefinition
    },
    variant: genVariant()
  };
}
