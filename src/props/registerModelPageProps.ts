import { DonutAppProps } from "../components/donutApp";
import { RegisterModelPageProps } from "../components/registerModelPage";
import { REGISTER_MODEL_PAGE } from "../definitions/model";
import { matchOptn } from "../tools";
import { UsageZoneProps } from "../components/registerModelPage/usageZone";
import { findNewMinor } from "../solution/modelManage";
import { summaryZoneProps } from "./summaryZoneProps";
import { sourceZoneProps } from "./sourceZoneProps";

export function registerModelPageProps(
  props: DonutAppProps
): RegisterModelPageProps {
  const mthds = props.methods;
  const state = props.state;
  let mtc = state.modelManagement.modelRegistering.match;

  return {
    summaryZone: summaryZoneProps(props),
    sourceZone: sourceZoneProps(props),

    type: REGISTER_MODEL_PAGE,

    handleSubmitRegisterModel: () =>
      mthds.workingPage.registerModel.handleSubmitRegisterModel(
        state.modelManagement.modelRegistering,
        state.modelManagement.modelRegistering.match,
        state.userAcnt.userID
      ),

    modelInfoZone: {
      authorIdField: state.modelManagement.modelRegistering.model.authorField,
      addressField: state.modelManagement.modelRegistering.model.addressField,
      modelDescription:
        state.modelManagement.modelRegistering.model.descriptionField,
      handleChangeAurhorField:
        mthds.workingPage.registerModel.handleChangeAurhorField,
      handleChangeAddressField:
        mthds.workingPage.registerModel.handleChangeAddressField,
      handleChangeModelDescription:
        mthds.workingPage.registerModel.handleChangeModelDescription
    },

    modelTypeZone: {
      modelType: matchOptn(mtc.modelType, {
        some: def => {
          return {
            nameField: def.knowledge.name,
            descriptionField: def.knowledge.description
          };
        },
        none: () => state.modelManagement.modelRegistering.modelType
      }),
      algo: matchOptn(mtc.algo, {
        some: def => {
          return {
            nameField: def.knowledge.name,
            descriptionField: def.knowledge.description
          };
        },
        none: () => state.modelManagement.modelRegistering.algo
      }),

      usageZone: usageZoneProps(props),
      dict: state.modelManagement.dict,
      matchModelType: state.modelManagement.modelRegistering.match.modelType,

      majorVer: state.modelManagement.modelRegistering.model.majorNum,
      newMajor: matchOptn(
        state.modelManagement.modelRegistering.match.modelType,
        {
          some: def => def.knowledge.version.newMajor,
          none: () => 0
        }
      ),
      newMinor: matchOptn(
        state.modelManagement.modelRegistering.match.modelType,
        {
          some: def => {
            // console.log('registerModelPagrProps newnimor.');
            // console.log(state.modelManagement.modelRegistering.model.majorNum);
            // console.log(def);
            // console.log(
            //     findNewMinor(state.modelManagement.modelRegistering.model.majorNum, def.knowledge.version)
            // );
            return findNewMinor(
              state.modelManagement.modelRegistering.model.majorNum,
              def.knowledge.version
            );
          },
          none: () => 0
        }
      ),

      handleChangeModelTypeName: (field: string) => {
        mthds.workingPage.registerModel.handleChangeModelTypeName(
          field,
          state.modelManagement.dict.modelTypeDict,
          state.modelManagement.modelRegistering.algo.nameField,
          state.modelManagement.dict.algoDict,
          state.modelManagement.modelRegistering.usage.nameField,
          state.modelManagement.dict.usageDict,
          state.modelManagement.modelRegistering.input.nameField,
          state.modelManagement.modelRegistering.output.nameField,
          state.modelManagement.dict.dataTypeDict
        );
      },

      handleChangeModelTypeDescription: (field: string) => {
        mthds.workingPage.registerModel.handleChangeModelTypeDescription(
          field,
          !mtc.modelType.isSome
        );
      },

      handleChangeAlgoName: (field: string) => {
        mthds.workingPage.registerModel.handleChangeAlgoName(
          field,
          !mtc.modelType.isSome,
          state.modelManagement.dict.algoDict
        );
      },

      handleChangeAlgoDesciption: (field: string) => {
        mthds.workingPage.registerModel.handleChangeAlgoDescription(
          field,
          !mtc.modelType.isSome && !mtc.algo.isSome
        );
      },

      handleSelectVersion: mthds.workingPage.registerModel.handleSelectVersion
    }
  }; // end return
}

function usageZoneProps(props: DonutAppProps): UsageZoneProps {
  const mthds = props.methods;
  const usageMthds = mthds.workingPage.registerModel.usage;
  const state = props.state;
  let mtc = state.modelManagement.modelRegistering.match;

  return {
    usageDisplay: matchOptn(mtc.usage, {
      some: def => {
        return {
          nameField: def.knowledge.name,
          descriptionField: def.knowledge.description
        };
      },
      none: () => state.modelManagement.modelRegistering.usage
    }),

    inputDisplay: matchOptn(mtc.input, {
      some: def => {
        return {
          nameField: def.knowledge.name,
          descriptionField: def.knowledge.description
        };
      },
      none: () => state.modelManagement.modelRegistering.input
    }),

    outputDisplay: matchOptn(mtc.output, {
      some: def => {
        return {
          nameField: def.knowledge.name,
          descriptionField: def.knowledge.description
        };
      },
      none: () => state.modelManagement.modelRegistering.output
    }),

    dict: state.modelManagement.dict,

    handleChangeUsageName: (field: string) => {
      usageMthds.handleChangeUsageName(
        field,
        !mtc.modelType.isSome,
        state.modelManagement.dict.usageDict,
        state.modelManagement.modelRegistering.input.nameField,
        state.modelManagement.modelRegistering.output.nameField,
        state.modelManagement.dict.dataTypeDict
      );
    },

    handleChangeUsageDescription: (field: string) => {
      usageMthds.handleChangeUsageDescription(
        field,
        !mtc.modelType.isSome && !mtc.usage.isSome
      );
    },

    handleChangeInputName: (field: string) => {
      usageMthds.handleChangeInputName(
        field,
        !mtc.modelType.isSome && !mtc.usage.isSome,
        state.modelManagement.dict.dataTypeDict
      );
    },

    handleChangeInputDescription: (field: string) => {
      usageMthds.handleChangeInputDescription(
        field,
        !mtc.modelType.isSome && !mtc.usage.isSome && !mtc.input.isSome
      );
    },

    handleChangeOutputName: (field: string) => {
      usageMthds.handleChangeOutputName(
        field,
        !mtc.modelType.isSome && !mtc.usage.isSome,
        state.modelManagement.dict.dataTypeDict
      );
    },

    handleChangeOutputDescription: (field: string) => {
      usageMthds.handleChangeOutputDescription(
        field,
        !mtc.modelType.isSome && !mtc.usage.isSome && !mtc.output.isSome
      );
    }
  };
}
