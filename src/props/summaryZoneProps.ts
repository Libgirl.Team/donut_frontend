import { SummaryZoneProps } from "../components/registerModelPage/summaryZone";
import { DonutAppProps } from "../components/donutApp";

export function summaryZoneProps(props: DonutAppProps): SummaryZoneProps {
  const state = props.state.modelManagement.modelRegistering.summary;
  const mthds = props.methods.workingPage.registerModel.summary;
  return {
    isShowingEval: state.isShowingEval,
    isShowingEff: state.isShowingEff,
    handleClickShowingEval: () => mthds.switchShowingEval(state.isShowingEval),
    handleClickShowingEff: () => mthds.switchShowingEff(state.isShowingEff),
    evalTable: {
      title: "Evaluations",
      rows: state.evalTable,
      addRow: () => mthds.addEvalRow(state.evalTable),
      handleEditlName: (n, nameField) =>
        mthds.editEvalName(
          state.evalTable,
          n,
          nameField,
          props.state.modelManagement.dict.evalDict
        ),
      handleEditValue: (n, valueField) =>
        mthds.editEvalValue(state.evalTable, n, valueField)
    },
    effTable: {
      title: "Efficiencies",
      rows: state.effTable,
      addRow: () => mthds.addEffRow(state.effTable),
      handleEditlName: (n, nameField) =>
        mthds.editEffName(
          state.effTable,
          n,
          nameField,
          props.state.modelManagement.dict.effDict
        ),
      handleEditValue: (n, valueField) =>
        mthds.editEffValue(state.effTable, n, valueField)
    }
  };
}
