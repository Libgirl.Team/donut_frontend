// import { EvalRegisteringState, EffRegisteringtate, MODEL_REGISTERING } from '../reducers/modelReducer';
import { SetSingleAction } from "./utils";

export const SET_AUTHOR_FIELD_ACTION = "SET_AUTHOR_FIELD_ACTION";

export interface SetAuthorFieldAction {
  type: typeof SET_AUTHOR_FIELD_ACTION;
  authorField: string;
}

export function setAuthorField(authorField: string): SetAuthorFieldAction {
  return {
    type: SET_AUTHOR_FIELD_ACTION,
    authorField
  };
}

export const SET_ADDRESS_FIELD_ACTION = "SET_ADDRESS_FIELD_ACTION";

export interface SetAddressFieldAction {
  type: typeof SET_ADDRESS_FIELD_ACTION;
  addressField: string;
}

export function setAddressField(addressField: string): SetAddressFieldAction {
  return {
    type: SET_ADDRESS_FIELD_ACTION,
    addressField
  };
}

export const SET_MODEL_DESCRIPTION_ACTION = "SET_MODEL_DESCRIPTION_ACTION";

export interface SetModelDescriptionAction {
  type: typeof SET_MODEL_DESCRIPTION_ACTION;
  descriptionField: string;
}

export function setModelDescriptionField(
  descriptionField: string
): SetModelDescriptionAction {
  return {
    type: SET_MODEL_DESCRIPTION_ACTION,
    descriptionField
  };
}

export const SET_VERSION = "SET_VERSION";

// export interface SetVersionAction {
//     type: typeof SET_VERSION,
//     majorNum: number,
//     minorNum: number,
// }

// export function setVersion(majorNum: number, minorNum: number): SetVersionAction {
//     return {
//         type: SET_VERSION,
//         majorNum,
//         minorNum,
//     }
// }

export const SET_MAJOR = "SET_MAJOR";
export const SET_MINOR = "SET_MINOR";

type SetMajorACtion = SetSingleAction<typeof SET_MAJOR, number>;
type SetMinorACtion = SetSingleAction<typeof SET_MINOR, number>;

export type ModelActionsSet =
  | SetAuthorFieldAction
  | SetAddressFieldAction
  | SetModelDescriptionAction
  //| SetVersionAction
  | SetMajorACtion
  | SetMinorACtion;
