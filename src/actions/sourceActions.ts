import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { SetSingleAction, filteredAct, setSingle } from "./utils";
import { matchOptn, optSome, optNone, Optn } from "../tools";
import {
  findDefByName,
  ModelTypeDict,
  majorVersions,
  minorVersions
} from "../solution/modelManage";
import { SET_MATCH_SOURCE_MODEL_TYPE } from "./workingPage/registerModelActions";
import { ModelIdState } from "../definitions/model";

export const SET_SOURCE_MODEL_TYPE = "SET_SOURCE_MODEL_TYPE";
export const SET_SOURCE_PARAMETERS = "SET_SOURCE_PARAMETERS";

type SetSourceModelTypeAction = SetSingleAction<
  typeof SET_SOURCE_MODEL_TYPE,
  string
>;
type SetSourceParamsAction = SetSingleAction<
  typeof SET_SOURCE_PARAMETERS,
  string
>;

export type SourceActionsSet = SetSourceModelTypeAction | SetSourceParamsAction;

export function handleChangePreModelType(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  filter: string,
  name: string,
  modelTypeDict: ModelTypeDict
) {
  return () => {
    dispatch(filteredAct(filter, setSingle(SET_SOURCE_MODEL_TYPE, name)));
    matchOptn(findDefByName(name, modelTypeDict), {
      some: def => {
        const majorNum: Optn<number> = (() => {
          const mjrVers = majorVersions(def);
          if (mjrVers.length === 0) {
            return optNone();
          } else {
            return optSome(mjrVers[mjrVers.length - 1]);
          }
        })();
        const minorNum: Optn<number> = (() => {
          if (majorNum.isSome) {
            const mnrVers = minorVersions(def, majorNum.v);
            if (mnrVers.length === 0) {
              return optNone();
            } else {
              return optSome(mnrVers[mnrVers.length - 1]);
            }
          } else {
            return optNone();
          }
        })();
        dispatch(
          filteredAct(
            filter,
            setSingle(
              SET_MATCH_SOURCE_MODEL_TYPE,
              optSome({
                modelType: def,
                majorNum,
                minorNum
              })
            )
          )
        );
      },
      none: () => {
        dispatch(
          filteredAct(filter, setSingle(SET_MATCH_SOURCE_MODEL_TYPE, optNone()))
        );
      }
    });
  };
}

export const SET_SOURCE_MAJOR = "SET_SOURCE_MAJOR";
export const SET_SOURCE_MINOR = "SET_SOURCE_MINOR";

export type SetSourceMajorAction = SetSingleAction<
  typeof SET_SOURCE_MAJOR,
  number
>; // should reset choosen minor
export type SetSourceMinorAction = SetSingleAction<
  typeof SET_SOURCE_MINOR,
  number
>;
