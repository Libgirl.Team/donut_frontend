import { SetFieldAction, filteredAct, setField, setSingle } from "./utils";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { DataTypeDict, findDefByName } from "../solution/modelManage";
import { matchOptn, optSome, optNone } from "../tools";
import {
  SET_MATCH_OUTPUT,
  SET_MATCH_INPUT
} from "./workingPage/registerModelActions";
import {
  MODEL_REGISTERING_OUTPUT,
  REGISTER_MODEL_PAGE,
  MODEL_REGISTERING_INPUT
} from "../definitions/model";

export const SET_DATA_TYPE_NAME = "SET_DATA_TYPE_NAME";
export const SET_DATA_TYPE_DESCRIPTION = "SET_DATA_TYPE_DESCRIPTION";

type SetDataTypeNameAction = SetFieldAction<typeof SET_DATA_TYPE_NAME>;
type SetDataTypeDescriptionAction = SetFieldAction<
  typeof SET_DATA_TYPE_DESCRIPTION
>;

export type DataTypeActionsSet =
  | SetDataTypeNameAction
  | SetDataTypeDescriptionAction;

export function handleChangeDataTypeName(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  dataTypeFilter: string,
  pageFilter: string,
  matchType: string,
  field: string,
  dataTypeDict: DataTypeDict
) {
  return () => {
    dispatch(filteredAct(dataTypeFilter, setField(SET_DATA_TYPE_NAME, field)));
    matchOptn(findDefByName(field, dataTypeDict), {
      some: def => {
        dispatch(filteredAct(pageFilter, setSingle(matchType, optSome(def))));
      },
      none: () => {
        dispatch(filteredAct(pageFilter, setSingle(matchType, optNone())));
      }
    });
  };
}

export function handleChangeRgstMdOutputName(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  field: string,
  dataTypeDict: DataTypeDict
) {
  return handleChangeDataTypeName(
    dispatch,
    MODEL_REGISTERING_OUTPUT,
    REGISTER_MODEL_PAGE,
    SET_MATCH_OUTPUT,
    field,
    dataTypeDict
  );
}

export function handleChangeRgstMdInputName(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  field: string,
  dataTypeDict: DataTypeDict
) {
  return handleChangeDataTypeName(
    dispatch,
    MODEL_REGISTERING_INPUT,
    REGISTER_MODEL_PAGE,
    SET_MATCH_INPUT,
    field,
    dataTypeDict
  );
}

// export function handleRegisterChangeOutputName(
//     dispatch: ThunkDispatch<{}, {}, AnyAction>,
//     field: string,
//     dataTypeDict: DataTypeDict,
// ) {
//     return () => {
//         dispatch(filteredAct(MODEL_REGISTERING_OUTPUT, setField(SET_DATA_TYPE_NAME, field)));
//         matchOptn(findDefByName(field, dataTypeDict), {
//             some: (def) => {
//                 dispatch(rgstMdSetSingle(SET_MATCH_OUTPUT, optSome(def)));
//             },
//             none: () => {
//                 dispatch(rgstMdSetSingle(SET_MATCH_OUTPUT, optNone()));
//             },
//         })
//     }
// }
