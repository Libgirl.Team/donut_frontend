import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  AcntsListPageState,
  ACNTS_LIST_PAGE
} from "../../reducers/workingPage/acntsListReducer";
import { getAcntsList } from "../../client/teamManagement";
import { authAct } from "../authPageActions";

export const SET_ACNTS_LIST_PAGE_ACTION = "SET_ACNTS_LIST_PAGE_ACTION";

export interface SetAcntsListPageAction {
  type: typeof SET_ACNTS_LIST_PAGE_ACTION;
  state: AcntsListPageState;
}

export function setAcntsListPage(
  state: AcntsListPageState
): SetAcntsListPageAction {
  return {
    type: SET_ACNTS_LIST_PAGE_ACTION,
    state
  };
}

export function gotoAcntsListPage(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return async () => {
    let resList = await getAcntsList();
    //console.log(resList);
    dispatch(
      authAct(dispatch, resList, list => {
        dispatch(
          setAcntsListPage({
            variant: ACNTS_LIST_PAGE,
            acntsList: list
          })
        );
      })
    );
  };
}

export type AcntsListActionsSet = SetAcntsListPageAction;
