import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  REGISTER_MODEL_PAGE,
  RegisterModelMatchState,
  DataTypeState,
  ModelIdState
} from "../../definitions/model";
import { updateModelMngDicts } from "../modelMngActions";
import {
  ModelTypeDef,
  UsageDef,
  AlgoDef,
  DataTypeDef,
  OwnerID,
  Model,
  EvalValue,
  EffValue,
  EvalDef,
  EffDef
} from "../../solution/modelManage";
import { filteredAct, setSingle, SetSingleAction } from "../utils";
import { Optn, optSome, matchOptn, optNone } from "../../tools";
import { ModelRegisteringState } from "../../reducers/modelManagementReducer";
import {
  postModel,
  postModelType,
  postAlgo,
  postUsage,
  postDataType,
  postEval,
  postEff
} from "../../client/modelMngClient";
import { authResponse } from "../authPageActions";
import { gotoModelsList } from "./modelsListActions";
import { SetSourceMajorAction, SetSourceMinorAction } from "../sourceActions";

export type RegisterModelActionsSet = GotoRegisterModelPageAction;

export const GOTO_REGISTER_MODEL_PAGE = "GOTO_REGISTER_MODEL_PAGE";

export interface GotoRegisterModelPageAction {
  variant: typeof REGISTER_MODEL_PAGE;
  type: typeof GOTO_REGISTER_MODEL_PAGE;
}

export function gotoRegisterModel(): GotoRegisterModelPageAction {
  return {
    type: GOTO_REGISTER_MODEL_PAGE,
    variant: REGISTER_MODEL_PAGE
  };
}

export function handleGotoRegisterModel(
  dispatch: ThunkDispatch<{}, {}, AnyAction>
) {
  return async () => {
    await updateModelMngDicts(dispatch);
    dispatch(gotoRegisterModel());
  };
}

// export type MatchModelTypeActionsSet = SetMatchModelTypeAction;

export const SET_MATCH_MODEL_TYPE = "SET_MATCH_MODEL_TYPE";
export const SET_MATCH_USAGE = "SET_MATCH_USAGE";
export const SET_MATCH_ALGO = "SET_MATCH_ALGO";
export const SET_MATCH_INPUT = "SET_MATCH_INPUT";
export const SET_MATCH_OUTPUT = "SET_MATCH_OUTUT";
export const SET_MATCH_SOURCE_MODEL_TYPE = "SET_MATCH_SOURCE_MODEL_TYPE";
type SetSourceMatchAction = SetSingleAction<
  typeof SET_MATCH_SOURCE_MODEL_TYPE,
  Optn<ModelIdState>
>;

export type SetModelMatchActions =
  | SetSingleAction<typeof SET_MATCH_MODEL_TYPE, Optn<ModelTypeDef>>
  | SetSingleAction<typeof SET_MATCH_USAGE, Optn<UsageDef>>
  | SetSingleAction<typeof SET_MATCH_ALGO, Optn<AlgoDef>>
  | SetSingleAction<typeof SET_MATCH_INPUT, Optn<DataTypeDef>>
  | SetSingleAction<typeof SET_MATCH_OUTPUT, Optn<DataTypeDef>>
  | SetSourceMatchAction
  | SetSourceMajorAction
  | SetSourceMinorAction;

export function rgstMdSetSingle<T, V>(type: T, v: V) {
  return filteredAct(REGISTER_MODEL_PAGE, setSingle(type, v));
}

export function handleSubmitRegisterModel(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  regState: ModelRegisteringState,
  match: RegisterModelMatchState,
  usrID: OwnerID
): () => Promise<void> {
  return async () => {
    async function postedDataType(
      dataState: DataTypeState
    ): Promise<Optn<DataTypeDef>> {
      let res = await postDataType({
        ownerID: usrID,
        name: dataState.nameField,
        description: dataState.descriptionField
      });
      return authResponse(dispatch, res);
    }

    async function postedUsage(
      input: Optn<DataTypeDef>,
      output: Optn<DataTypeDef>
    ): Promise<Optn<UsageDef>> {
      if (input.isSome && output.isSome) {
        let res = await postUsage({
          ownerID: usrID,
          name: regState.usage.nameField,
          description: regState.usage.descriptionField,
          input: input.v._id,
          output: output.v._id
        });
        return authResponse(dispatch, res);
      } else {
        return optNone();
      }
    }

    async function postedAlgo(): Promise<Optn<AlgoDef>> {
      let res = await postAlgo({
        ownerID: usrID,
        name: regState.algo.nameField,
        description: regState.algo.descriptionField
      });
      return authResponse(dispatch, res);
    }

    async function postedModelType(
      algo: Optn<AlgoDef>,
      usage: Optn<UsageDef>
    ): Promise<Optn<ModelTypeDef>> {
      if (algo.isSome === true && usage.isSome === true) {
        let res = await postModelType({
          ownerID: usrID,
          name: regState.modelType.nameField,
          description: regState.modelType.descriptionField,
          algo: algo.v._id,
          usage: usage.v._id
        });
        return authResponse(dispatch, res);
      } else {
        return optNone();
      }
    }

    async function postedEval(evalName: string): Promise<Optn<EvalDef>> {
      const res = await postEval({
        ownerID: usrID,
        name: evalName,
        description: "",
        unit: "",
        dataset: ""
      });
      return authResponse(dispatch, res);
    }

    async function postedEff(effName: string): Promise<Optn<EffDef>> {
      const res = await postEff({
        ownerID: usrID,
        name: effName,
        description: "",
        unit: ""
      });
      return authResponse(dispatch, res);
    }

    async function postedModel(mdt: Optn<ModelTypeDef>): Promise<Optn<Model>> {
      if (mdt.isSome) {
        let res = await postModel(regState.model.majorNum, {
          ownerID: usrID,
          authorID: optSome(regState.model.authorField),
          description: regState.model.descriptionField,
          address: regState.model.addressField,
          tags: [],
          source: {
            previousModel: "",
            params: "",
            dataset: ""
          },
          intrinsics: { modelType: mdt.v._id },
          summary: {
            eval: await regState.summary.evalTable.reduce(
              async (acc: Promise<EvalValue[]>, x) => {
                if (x.matchedItem.isSome && !(x.valueField === "")) {
                  return (await acc).concat([
                    {
                      _id: x.matchedItem.v,
                      value: (x.valueField as unknown) as number
                    }
                  ]);
                } else if (!(x.nameField === "") && !(x.valueField === "")) {
                  return await matchOptn(await postedEval(x.nameField), {
                    some: async def =>
                      (await acc).concat([
                        {
                          _id: def._id,
                          value: (x.valueField as unknown) as number
                        }
                      ]),
                    none: async () => await acc
                  });
                } else {
                  return await acc;
                }
              },
              Promise.resolve([])
            ),

            eff: await regState.summary.effTable.reduce(
              async (acc: Promise<EffValue[]>, x) => {
                if (x.matchedItem.isSome && !(x.valueField === "")) {
                  return (await acc).concat([
                    {
                      _id: x.matchedItem.v,
                      value: (x.valueField as unknown) as number
                    }
                  ]);
                } else if (!(x.nameField === "") && !(x.valueField === "")) {
                  return await matchOptn(await postedEff(x.nameField), {
                    some: async def =>
                      (await acc).concat([
                        {
                          _id: def._id,
                          value: (x.valueField as unknown) as number
                        }
                      ]),
                    none: async () => await acc
                  });
                } else {
                  return await acc;
                }
              },
              Promise.resolve([])
            )
          }
        });
        return authResponse(dispatch, res);
      } else {
        return optNone();
      }
    }

    await matchOptn(
      await postedModel(
        await matchOptn(match.modelType, {
          some: async mdt => optSome(mdt),
          none: async () =>
            await postedModelType(
              await matchOptn(match.algo, {
                some: async algo => optSome(algo),
                none: async () => await postedAlgo()
              }),
              await matchOptn(match.usage, {
                some: async usg => optSome(usg),
                none: async () =>
                  await postedUsage(
                    await matchOptn(match.input, {
                      some: async input => optSome(input),
                      none: async () => await postedDataType(regState.input)
                    }),
                    await matchOptn(match.output, {
                      some: async output => optSome(output),
                      none: async () => await postedDataType(regState.output)
                    })
                  )
              })
            )
        })
      ),
      {
        some: async _ => dispatch(gotoModelsList(dispatch)),
        none: async () => {}
      }
    );
  };
}
