import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  MODELS_LIST_PAGE,
  ModelsListPageState
} from "../../reducers/workingPage/modelsListReducer";
import { updateModelMngDicts } from "../modelMngActions";

export type ModelsListActionsSet = SetModelsListPageAction;

export const SET_MODELS_LIST_ACTION = "SET_MODELS_LIST_ACTION";

export interface SetModelsListPageAction {
  type: typeof SET_MODELS_LIST_ACTION;
  state: ModelsListPageState;
}

export function setModelsListPage(
  state: ModelsListPageState
): SetModelsListPageAction {
  return {
    type: SET_MODELS_LIST_ACTION,
    state
  };
}

export const GOTO_MODELS_LIST_PAGE = "GOTO_MODELS_LIST_PAGE";

export interface GotoModelsListAction {
  variant: typeof MODELS_LIST_PAGE;
  type: typeof GOTO_MODELS_LIST_PAGE;
}

export function gotoModelsList(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return async () => {
    await updateModelMngDicts(dispatch);
    dispatch(
      setModelsListPage({
        variant: MODELS_LIST_PAGE
      })
    );
  };
}
