import { SINGLE_MODEL_PAGE } from "../../reducers/workingPage/singleModelPageReducer";

export type SingleModelActionsSet = GotoSingleModelPageAction;

export const GOTO_SINGLE_MODEL_PAGE = "GOTO_SINGLE_MODEL_PAGE";

export interface GotoSingleModelPageAction {
  variant: typeof SINGLE_MODEL_PAGE;
  type: typeof GOTO_SINGLE_MODEL_PAGE;
}

export function gotoSingleModel(): GotoSingleModelPageAction {
  return {
    type: GOTO_SINGLE_MODEL_PAGE,
    variant: SINGLE_MODEL_PAGE
  };
}
