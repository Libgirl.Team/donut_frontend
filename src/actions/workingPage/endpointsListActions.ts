import { ENDPOINTS_LIST_PAGE } from "../../reducers/workingPage/endpointsListReducer";

export type EndpointsListActionsSet = GotoEndpointsListAction;

export const GOTO_ENDPOINTS_LIST_PAGE = "GOTO_ENDPOINTS_LIST_PAGE";

export interface GotoEndpointsListAction {
  variant: typeof ENDPOINTS_LIST_PAGE;
  type: typeof GOTO_ENDPOINTS_LIST_PAGE;
}
