import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  DEFINITION_PAGE,
  DefinitionPageState
} from "../../reducers/workingPage/definitionReducer";

export type DefinitionActionsSet = SetDefinitionAction;

export const SET_DEFINITION_ACTION = "SET_DEFINITION_ACTION";

export interface SetDefinitionAction {
  type: typeof SET_DEFINITION_ACTION;
  state: DefinitionPageState;
}

export function setDefinitionPage(
  state: DefinitionPageState
): SetDefinitionAction {
  return {
    type: SET_DEFINITION_ACTION,
    state
  };
}

export const GOTO_DEFINITION_PAGE = "GOTO_DEFINITION_PAGE";

export function gotoDefinitionPage(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return () => {
    dispatch(
      setDefinitionPage({
        variant: DEFINITION_PAGE
      })
    );
  };
}
