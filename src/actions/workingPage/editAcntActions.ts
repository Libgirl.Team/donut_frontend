import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  EditAcntPageState,
  EDIT_ACNT_PAGE
} from "../../reducers/workingPage/editAcntReducer";
import { Acnt, MMLv, DMLv, tmToPub, PubLv } from "../../solution/teamManage";
import { putAccountLevel, deleteAccount } from "../../client/teamManagement";
import { gotoAcntsListPage } from "./acntsListPageActions";

export type EditAcntActionsSet =
  | SetEditAcntPageAction
  | SetTMChoiceAction
  | SetMMChoiceAction
  | SetDMChoiceAction;

export const SET_EDIT_ACNT_ACTION = "SET_EDIT_ACNT_ACTION";

export interface SetEditAcntPageAction {
  type: typeof SET_EDIT_ACNT_ACTION;
  state: EditAcntPageState;
}

export function setEditAcntPage(
  state: EditAcntPageState
): SetEditAcntPageAction {
  return {
    type: SET_EDIT_ACNT_ACTION,
    state
  };
}

// export const GOTO_EDIT_ACNT_PAGE = 'GOTO_EDIT_ACNT_PAGE';

export function gotoEditAcnt(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  target: Acnt
) {
  return () => {
    if (target.tmLevel <= 3) {
      dispatch(
        setEditAcntPage({
          variant: EDIT_ACNT_PAGE,
          target,
          tmChoice: tmToPub(target.tmLevel),
          mmChoice: target.mmLevel,
          dmChoice: target.dmLevel
        })
      );
    } else {
      console.log("goto Edit Super!");
    }
  };
}

export function handleSubmitEditAcnt(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  target: string,
  tmLv: PubLv,
  mmLv: MMLv,
  dmLv: DMLv
) {
  return async () => {
    await putAccountLevel(target, tmLv, mmLv, dmLv);
    dispatch(gotoAcntsListPage(dispatch));
  };
}

export const SET_TM_CHOICE = "SET_TM_CHOICE";

export interface SetTMChoiceAction {
  type: typeof SET_TM_CHOICE;
  tmChoice: PubLv;
  mmChoice: MMLv;
  dmChoice: DMLv;
}

export function setTMChoice(
  tmChoice: PubLv,
  mmChoice: MMLv,
  dmChoice: DMLv
): SetTMChoiceAction {
  // console.log('setTMChoice tmChoice:');
  // console.log(tmChoice);
  if (tmChoice === 0) {
    return {
      type: SET_TM_CHOICE,
      tmChoice,
      mmChoice: tmChoice,
      dmChoice: tmChoice
    };
  } else if (tmChoice >= 2) {
    return {
      type: SET_TM_CHOICE,
      tmChoice,
      mmChoice: tmChoice > mmChoice ? tmChoice : mmChoice,
      dmChoice: tmChoice > dmChoice ? tmChoice : dmChoice
    };
  } else {
    return {
      type: SET_TM_CHOICE,
      tmChoice,
      mmChoice,
      dmChoice
    };
  }
}

export const SET_MM_CHOICE = "SET_MM_CHOICE";

export interface SetMMChoiceAction {
  type: typeof SET_MM_CHOICE;
  mmChoice: PubLv;
}

export function setMMChoice(mmChoice: PubLv): SetMMChoiceAction {
  return {
    type: SET_MM_CHOICE,
    mmChoice
  };
}

export const SET_DM_CHOICE = "SET_DM_CHOICE";

export interface SetDMChoiceAction {
  type: typeof SET_DM_CHOICE;
  dmChoice: PubLv;
}

export function setDMChoice(dmChoice: PubLv): SetDMChoiceAction {
  return {
    type: SET_DM_CHOICE,
    dmChoice
  };
}

export function handleDeleteAcnt(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  target: string
) {
  return async () => {
    await deleteAccount(target);
    dispatch(gotoAcntsListPage(dispatch));
  };
}
