import { AnyAction } from "redux";

export interface FilteredAction {
  filter: string;
  type: string;
}

export function filteredAct(filter: string, action: AnyAction): FilteredAction {
  return {
    ...action,
    filter
  };
}

export interface SetFieldAction<T> {
  type: T;
  field: string;
}

export function setField<T>(type: T, field: string): SetFieldAction<T> {
  return { type, field };
}

// export interface SetDictAction<T, D> {
//     type: T,
//     dict: D,
// }

// export function setDict<T, U>(type: T, dict: U) {
//     return {type, dict};
// }

export interface SetSingleAction<T, V> {
  type: T;
  v: V;
}

export function setSingle<T, V>(type: T, v: V): SetSingleAction<T, V> {
  return { type, v };
}
