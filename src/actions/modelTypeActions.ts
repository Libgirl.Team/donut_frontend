import { SetFieldAction, filteredAct, setField, setSingle } from "./utils";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  ModelTypeDict,
  AlgoDict,
  UsageDict,
  DataTypeDict,
  findDefByName,
  findDefByID,
  UsageDef
} from "../solution/modelManage";
import { matchOptn, optSome, Optn, optNone } from "../tools";
import {
  SET_MATCH_MODEL_TYPE,
  SET_MATCH_ALGO,
  SET_MATCH_USAGE,
  SET_MATCH_INPUT,
  SET_MATCH_OUTPUT
} from "./workingPage/registerModelActions";
import { SET_MAJOR } from "./modelAction";
import { handleChangeAlgoName } from "./algoActions";
import { handleChangeUsageName } from "./usageActions";

export type ModelTypeActionsSet =
  | SetModelTypeNameAction
  | SetModelTypeDescriptionAction;

export const SET_MODEL_TYPE_NAME = "SET_MODEL_TYPE_NAME";
export const SET_MODEL_TYPE_DESCRIPTION = "SET_MODEL_TYPE_DESCRIPTION";

type SetModelTypeNameAction = SetFieldAction<typeof SET_MODEL_TYPE_NAME>;
type SetModelTypeDescriptionAction = SetFieldAction<
  typeof SET_MODEL_TYPE_DESCRIPTION
>;

export function handleChangeModelTypeName(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  filter: string,
  modelTypeField: string,
  modelTypeDict: ModelTypeDict,
  editingAlgo: string,
  algoDict: AlgoDict,
  editingUsage: string,
  usageDict: UsageDict,
  editingInput: string,
  editingOutput: string,
  dataTypeDict: DataTypeDict
) {
  return () => {
    dispatch(
      filteredAct(filter, setField(SET_MODEL_TYPE_NAME, modelTypeField))
    );
    matchOptn(findDefByName(modelTypeField, modelTypeDict), {
      some: def => {
        //console.log('handleChangeModelTypeName found matching.');
        dispatch(
          filteredAct(filter, setSingle(SET_MATCH_MODEL_TYPE, optSome(def)))
        );
        dispatch(
          filteredAct(
            filter,
            setSingle(SET_MAJOR, def.knowledge.version.newMajor)
          )
        ); // default newMajor of the matched modeltype.
        dispatch(
          filteredAct(
            filter,
            setSingle(SET_MATCH_ALGO, findDefByID(def.knowledge.algo, algoDict))
          )
        );
        const matchedUsageDef: Optn<UsageDef> = findDefByID(
          def.knowledge.usage,
          usageDict
        );
        dispatch(
          filteredAct(filter, setSingle(SET_MATCH_USAGE, matchedUsageDef))
        );
        matchOptn(matchedUsageDef, {
          some: def => {
            dispatch(
              filteredAct(
                filter,
                setSingle(
                  SET_MATCH_INPUT,
                  findDefByID(def.knowledge.input, dataTypeDict)
                )
              )
            );
            dispatch(
              filteredAct(
                filter,
                setSingle(
                  SET_MATCH_OUTPUT,
                  findDefByID(def.knowledge.output, dataTypeDict)
                )
              )
            );
          },
          none: () => {}
        });
      },
      none: () => {
        dispatch(filteredAct(filter, setSingle(SET_MAJOR, 0)));
        dispatch(
          filteredAct(filter, setSingle(SET_MATCH_MODEL_TYPE, optNone()))
        );
        dispatch(handleChangeAlgoName(dispatch, filter, editingAlgo, algoDict));
        dispatch(
          handleChangeUsageName(
            dispatch,
            filter,
            editingUsage,
            usageDict,
            editingInput,
            editingOutput,
            dataTypeDict
          )
        );
      }
    });
  };
}
