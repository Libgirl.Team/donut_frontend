import { SetSingleAction, setSingle } from "./utils";
import {
  EvalRegisteringState,
  EffRegisteringState,
  ItemRegisteringState,
  initEvalRegisteringState,
  initEffRegisteringState
} from "../definitions/model";
import { matchOptn, optSome, optNone, Optn } from "../tools";
import {
  findDefByName,
  EvalDict,
  EffDict,
  EffItem,
  EffDef,
  EvalDef,
  EvalItem
} from "../solution/modelManage";

export const SET_SHOWING_EVAL = "SET_SHOWING_EVAL";
export const SET_SHOWING_EFF = "SET_SHOWING_EFF";

type SetShowingEvalAction = SetSingleAction<typeof SET_SHOWING_EVAL, boolean>;
type SetShowingEffAction = SetSingleAction<typeof SET_SHOWING_EFF, boolean>;

export const SET_EVAL_TABLE = "SET_EVAL_TABLE";

type SetEvalTableAction = SetSingleAction<
  typeof SET_EVAL_TABLE,
  Array<EvalRegisteringState>
>;

export const SET_EFF_TABLE = "SET_EFF_TABLE";

type SetEffTableAction = SetSingleAction<
  typeof SET_EFF_TABLE,
  Array<EffRegisteringState>
>;

export function addEvalRow(evalTable: EvalRegisteringState[]) {
  return setSingle(
    SET_EVAL_TABLE,
    evalTable.concat([initEvalRegisteringState])
  );
}

export function addEffRow(effTable: EffRegisteringState[]) {
  return setSingle(SET_EFF_TABLE, effTable.concat([initEffRegisteringState]));
}

function nameEditedEvals(
  table: ItemRegisteringState[],
  n: number,
  nameField: string,
  dict: EvalDict
): ItemRegisteringState[] {
  if (table[n]) {
    table[n] = {
      ...table[n],
      matchedItem: matchOptn<EvalDef, Optn<EvalItem>>(
        findDefByName(nameField, dict),
        {
          some: def => {
            console.log(`nameEditedEvals found ${nameField}`);
            return optSome(def._id);
          },
          none: () => optNone()
        }
      ),
      nameField
    };
  } else {
    console.log("editName: n not in table!");
  }
  return table;
}

export const editEvalName = (
  table: EvalRegisteringState[],
  n: number,
  nameField: string,
  dict: EvalDict
) => setSingle(SET_EVAL_TABLE, nameEditedEvals(table, n, nameField, dict));

function nameEditedEffs(
  table: ItemRegisteringState[],
  n: number,
  nameField: string,
  dict: EffDict
): ItemRegisteringState[] {
  if (table[n]) {
    table[n] = {
      ...table[n],
      matchedItem: matchOptn<EffDef, Optn<EffItem>>(
        findDefByName(nameField, dict),
        {
          some: def => optSome(def._id),
          none: () => optNone()
        }
      ),
      nameField
    };
  } else {
    console.log("editName: n not in table!");
  }
  return table;
}

export const editEffName = (
  table: EffRegisteringState[],
  n: number,
  nameField: string,
  dict: EffDict
) => setSingle(SET_EFF_TABLE, nameEditedEffs(table, n, nameField, dict));

function editValue(
  table: ItemRegisteringState[],
  n: number,
  valueField: string
): ItemRegisteringState[] {
  if (table[n]) {
    const toNum = Number(valueField);
    if (isFinite(toNum) && valueField.indexOf(" ") < 0) {
      table[n] = {
        ...table[n],
        valueField
      };
    }
  } else {
    console.log("editValue: n not in table!");
  }
  return table;
}

export const editEvalValue = (
  table: EvalRegisteringState[],
  n: number,
  valueField: string
) => setSingle(SET_EVAL_TABLE, editValue(table, n, valueField));
export const editEffValue = (
  table: EffRegisteringState[],
  n: number,
  valueField: string
) => setSingle(SET_EFF_TABLE, editValue(table, n, valueField));

export type SummaryActionsSet =
  | SetShowingEvalAction
  | SetShowingEffAction
  | SetEvalTableAction
  | SetEffTableAction;
