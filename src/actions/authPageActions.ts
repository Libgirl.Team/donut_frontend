import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { AuthState } from "../reducers/authReducer";
import { ResResult, OTHER_ERR } from "../client/client";
import { googleLogin, logout } from "../client/auth";
import { getUserAcnt } from "../client/teamManagement";
import { setUserAcnt } from "./userAcntActions";
import {
  gotoAcntsListPage,
  setAcntsListPage
} from "./workingPage/acntsListPageActions";
import { defaultAcnt } from "../solution/teamManage";
import { initialAcntsListPageState } from "../reducers/workingPage/acntsListReducer";
import { Optn, OptNone, OptSome, optNone, optSome } from "../tools";

export const SET_AUTH_STATE = "SET_AUTH_STATE";

export type AuthActionsSet = SetAuthStateAction;

export interface SetAuthStateAction {
  type: typeof SET_AUTH_STATE;
  authState: AuthState;
}

export function setAuthState(authState: AuthState): SetAuthStateAction {
  return {
    type: SET_AUTH_STATE,
    authState
  };
}

export function authAct<T>(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  res: ResResult<T>,
  act?: (body: T) => void
) {
  return () => {
    if (res === "401") {
      console.log("got 401 error.");
      dispatch(setAuthState(false));
    } else if (res === OTHER_ERR) {
      console.log("got other request error.");
    } else {
      if (act) {
        dispatch(setAuthState(true));
        act(res);
      }
    }
  };
}

export async function authProgn<T>(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  res: ResResult<T>,
  progn: (v: T) => Promise<void>
) {
  if (res === "401") {
    console.log("authProgn got 401 error.");
    dispatch(setAuthState(false));
  } else if (res === OTHER_ERR) {
    console.log("got other request error.");
  } else {
    await progn(res);
  }
}

export function authResponse<T>(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  res: ResResult<T>
): Optn<T> {
  if (res === "401") {
    console.log("authResponse got 401 error.");
    dispatch(setAuthState(false));
    return optNone();
  } else if (res === OTHER_ERR) {
    console.log("got other request error.");
    return optNone();
  } else {
    return optSome(res);
  }
}

export function handleGoogleLogin(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return async () => {
    // console.log("handleGoogleLogin:");
    await googleLogin();

    let userAcnt = await getUserAcnt();

    dispatch(
      authAct(dispatch, userAcnt, acnt => {
        dispatch(setUserAcnt(acnt));
        dispatch(setAuthState(true));
        dispatch(gotoAcntsListPage(dispatch));
      })
    );
  };
}

export function handleLogout(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return async () => {
    dispatch(authAct(dispatch, await logout()));
    dispatch(
      authAct(dispatch, await logout(), () => {
        // reset app state.
        dispatch(setUserAcnt(defaultAcnt));
        dispatch(setAcntsListPage(initialAcntsListPageState));
      })
    ); // try logout again to use authAct to go to login page.
  };
}
