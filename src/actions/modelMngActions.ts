import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  ModelTypeDict,
  UsageDict,
  DataTypeDict,
  EvalDict,
  EffDict,
  ModelDict,
  DatasetDict
} from "../solution/modelManage";
import {
  getModelTypeDefList,
  getAlgoDefList,
  getUsageDefList,
  getDataTypeDefList,
  getEvalDefList,
  getEffDefList,
  getModelList,
  getDatasetDeflist
} from "../client/modelMngClient";
import { authAct } from "./authPageActions";
import { SetSingleAction, setSingle } from "./utils";
import { ResResult } from "../client/client";

export type ModelMngActionsSet =
  | SetModelTypeDictAction
  | SetAlgoDictAction
  | SetUsageDictAction
  | SetDataTypeDictAction
  | SetEvalDictAction
  | SetEffDictAction
  | SetModelDictAction
  | SetDatasetDictACtion;

async function updateDict<T, V>(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  type: T,
  getter: Promise<ResResult<Array<V>>>
) {
  let reslist = await getter;
  return () => {
    dispatch(
      authAct(dispatch, reslist, dict => {
        // if (type as unknown as string == SET_MODEL_TYPE_DICT) {
        //     console.log(`updateDict ${type} dict:`);
        //     console.log(dict);
        // };
        // console.log(`updateDict ${type} dict:`);
        // console.log(dict);
        dispatch(setSingle(type, dict));
      })
    );
  };
}

export const updateModelDict = (dispatch: ThunkDispatch<{}, {}, AnyAction>) =>
  updateDict(dispatch, SET_MODEL_DICT, getModelList());

export const updateModelTypeDict = (
  dispatch: ThunkDispatch<{}, {}, AnyAction>
) => updateDict(dispatch, SET_MODEL_TYPE_DICT, getModelTypeDefList());

export const updateAlgoDict = (dispatch: ThunkDispatch<{}, {}, AnyAction>) =>
  updateDict(dispatch, SET_ALGO_DICT, getAlgoDefList());

export const updateUsageDict = (dispatch: ThunkDispatch<{}, {}, AnyAction>) =>
  updateDict(dispatch, SET_USAGE_DICT, getUsageDefList());

export const updateDataTypeDict = (
  dispatch: ThunkDispatch<{}, {}, AnyAction>
) => updateDict(dispatch, SET_DATA_TYPE_DICT, getDataTypeDefList());

export const updateEvalDict = (dispatch: ThunkDispatch<{}, {}, AnyAction>) =>
  updateDict(dispatch, SET_EVAL_DICT, getEvalDefList());

export const updateEffDict = (dispatch: ThunkDispatch<{}, {}, AnyAction>) =>
  updateDict(dispatch, SET_EFF_DICT, getEffDefList());

export const updateDatasetDict = (dispatch: ThunkDispatch<{}, {}, AnyAction>) =>
  updateDict(dispatch, SET_DATASET_DICT, getDatasetDeflist());

export async function updateModelMngDicts(
  dispatch: ThunkDispatch<{}, {}, AnyAction>
) {
  dispatch(await updateModelTypeDict(dispatch));
  dispatch(await updateModelDict(dispatch));
  dispatch(await updateAlgoDict(dispatch));
  dispatch(await updateUsageDict(dispatch));
  dispatch(await updateDataTypeDict(dispatch));
  dispatch(await updateEffDict(dispatch));
  dispatch(await updateEvalDict(dispatch));
  dispatch(await updateDatasetDict(dispatch));
}

export const SET_MODEL_DICT = "SET_MODEL_DICT";
type SetModelDictAction = SetSingleAction<typeof SET_MODEL_DICT, ModelDict>;

export const SET_MODEL_TYPE_DICT = "SET_MODEL_TYPE_DICT";
type SetModelTypeDictAction = SetSingleAction<
  typeof SET_MODEL_TYPE_DICT,
  ModelTypeDict
>;

export const SET_ALGO_DICT = "SET_ALGO_DICT";
type SetAlgoDictAction = SetSingleAction<typeof SET_ALGO_DICT, ModelTypeDict>;

export const SET_USAGE_DICT = "SET_USAGE_DICT";
type SetUsageDictAction = SetSingleAction<typeof SET_USAGE_DICT, UsageDict>;

export const SET_DATA_TYPE_DICT = "SET_DATA_TYPE_DICT";
type SetDataTypeDictAction = SetSingleAction<
  typeof SET_DATA_TYPE_DICT,
  DataTypeDict
>;

export const SET_EVAL_DICT = "SET_EVAL_DICT";
type SetEvalDictAction = SetSingleAction<typeof SET_EVAL_DICT, EvalDict>;

export const SET_EFF_DICT = "SET_EFF_DICT";
type SetEffDictAction = SetSingleAction<typeof SET_EFF_DICT, EffDict>;

export const SET_DATASET_DICT = "SET_DATASET_DICT";
type SetDatasetDictACtion = SetSingleAction<
  typeof SET_DATASET_DICT,
  DatasetDict
>;
