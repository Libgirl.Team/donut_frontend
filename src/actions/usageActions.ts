import { SetFieldAction, filteredAct, setField, setSingle } from "./utils";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import {
  UsageDict,
  DataTypeDict,
  findDefByName,
  findDefByID
} from "../solution/modelManage";
import { matchOptn, optSome, optNone } from "../tools";
import {
  SET_MATCH_USAGE,
  SET_MATCH_INPUT,
  SET_MATCH_OUTPUT
} from "./workingPage/registerModelActions";
import {
  handleChangeDataTypeName,
  handleChangeRgstMdInputName,
  handleChangeRgstMdOutputName
} from "./dataTypeActions";

export const SET_USAGE_NAME = "SET_USAGE_NAME";
export const SET_USAGE_DESCRIPTION = "SET_USAGE_DESCRIPTION";

type SetUsageNameAction = SetFieldAction<typeof SET_USAGE_NAME>;
type SetUsageDescriptionAction = SetFieldAction<typeof SET_USAGE_DESCRIPTION>;

export type UsageActionsSet = SetUsageDescriptionAction | SetUsageNameAction;

export function handleChangeUsageName(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  filter: string,
  field: string,
  usageDict: UsageDict,
  editingInput: string,
  editingOutput: string,
  dataTypeDict: DataTypeDict
) {
  return () => {
    dispatch(filteredAct(filter, setField(SET_USAGE_NAME, field)));
    matchOptn(findDefByName(field, usageDict), {
      some: def => {
        dispatch(filteredAct(filter, setSingle(SET_MATCH_USAGE, optSome(def))));
        dispatch(
          filteredAct(
            filter,
            setSingle(
              SET_MATCH_INPUT,
              findDefByID(def.knowledge.input, dataTypeDict)
            )
          )
        );
        dispatch(
          filteredAct(
            filter,
            setSingle(
              SET_MATCH_OUTPUT,
              findDefByID(def.knowledge.output, dataTypeDict)
            )
          )
        );
      },
      none: () => {
        dispatch(filteredAct(filter, setSingle(SET_MATCH_USAGE, optNone())));
        dispatch(
          handleChangeRgstMdInputName(dispatch, editingInput, dataTypeDict)
        );
        dispatch(
          handleChangeRgstMdOutputName(dispatch, editingOutput, dataTypeDict)
        );
      }
    });
  };
}
