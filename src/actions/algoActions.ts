import { SetFieldAction, filteredAct, setField, setSingle } from "./utils";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { AlgoDict, findDefByName } from "../solution/modelManage";
import { matchOptn, optSome, optNone } from "../tools";
import { SET_MATCH_ALGO } from "./workingPage/registerModelActions";

export const SET_ALGO_NAME = "SET_ALGO_NAME";
export const SET_ALGO_DESCRIPTION = "SET_ALGO_DESCRIPTION";

type SetAlgoNameAction = SetFieldAction<typeof SET_ALGO_NAME>;
type SetAlgoDescriptionAction = SetFieldAction<typeof SET_ALGO_DESCRIPTION>;

export type AlgoActionsSet = SetAlgoNameAction | SetAlgoDescriptionAction;

export function handleChangeAlgoName(
  dispatch: ThunkDispatch<{}, {}, AnyAction>,
  filter: string,
  field: string,
  algoDict: AlgoDict
) {
  return () => {
    dispatch(filteredAct(filter, setField(SET_ALGO_NAME, field)));
    matchOptn(findDefByName(field, algoDict), {
      some: def => {
        dispatch(filteredAct(filter, setSingle(SET_MATCH_ALGO, optSome(def))));
      },
      none: () => {
        dispatch(filteredAct(filter, setSingle(SET_MATCH_ALGO, optNone())));
      }
    });
  };
}
