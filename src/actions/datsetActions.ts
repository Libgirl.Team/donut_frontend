import { SetSingleAction } from "./utils";

export const SET_DATASET_NAME = "SET_DATASET_NAME";
export const SET_DATASET_DESCRIPTION = "SET_DATASET_DESCRIPTION";
export const SET_DATASET_URL = "SET_DATASET";

type SetDatasetNameAction = SetSingleAction<typeof SET_DATASET_NAME, string>;
type SetDatasetDescriptionAction = SetSingleAction<
  typeof SET_DATASET_DESCRIPTION,
  string
>;
type SetDatasetUrlAction = SetSingleAction<typeof SET_DATASET_URL, string>;

export type DatasetActionsSet =
  | SetDatasetNameAction
  | SetDatasetDescriptionAction
  | SetDatasetUrlAction;
