import { UserAcntState } from "../reducers/userAcntReducer";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { getUserAcnt } from "../client/teamManagement";
import { authAct } from "./authPageActions";

export const SET_USER_ACNT = "SET_USER_ACNT";

export interface SetUserAcntAction {
  type: typeof SET_USER_ACNT;
  userAcnt: UserAcntState;
}

export function setUserAcnt(userAcnt: UserAcntState): SetUserAcntAction {
  return {
    type: SET_USER_ACNT,
    userAcnt
  };
}

export function updateUserAcnt(dispatch: ThunkDispatch<{}, {}, AnyAction>) {
  return async () => {
    let userAcnt = await getUserAcnt();

    dispatch(
      authAct(dispatch, userAcnt, acnt => {
        dispatch(setUserAcnt(acnt));
      })
    );
  };
}

export type UserAcntActionsSet = SetUserAcntAction;
