import {
  EvalItem,
  EffItem,
  ModelTypeDef,
  AlgoDef,
  UsageDef,
  DataTypeDef,
  ModelDict,
  ModelTypeDict,
  AlgoDict,
  UsageDict,
  DataTypeDict,
  EvalDict,
  EffDict,
  Item,
  DatasetDict,
  VersionPair
} from "../solution/modelManage";
import { Optn, optNone } from "../tools";

export const REGISTER_MODEL_PAGE = "REGISTER_MODEL_PAGE";
export const MODEL_REGISTERING_INPUT = "MODEL_REGISTERING_INPUT";
export const MODEL_REGISTERING_OUTPUT = "MODEL_REGISTERING_OUTPUT";
export const MODEL_TYPE_EDITING = "MODEL_TYPE_EDITING";
export const ALGO_EDITING = "ALGO_EDITING";
export const USAGE_EDITING = "ALGO_EDITING";

export interface ItemRegisteringState {
  matchedItem: Optn<Item>;
  nameField: string;
  valueField: string;
}

export const initItemResgisteringState: ItemRegisteringState = {
  matchedItem: optNone(),
  nameField: "",
  valueField: ""
};

export type EvalRegisteringState = ItemRegisteringState;
export const initEvalRegisteringState: EvalRegisteringState = initItemResgisteringState;
export type EffRegisteringState = ItemRegisteringState;
export const initEffRegisteringState: EffRegisteringState = initItemResgisteringState;

export interface SummaryState {
  isShowingEval: boolean;
  isShowingEff: boolean;
  evalTable: Array<EvalRegisteringState>;
  effTable: Array<EffRegisteringState>;
}

export const initSummaryState: SummaryState = {
  isShowingEval: true,
  isShowingEff: true,
  evalTable: [initEvalRegisteringState],
  effTable: [initEffRegisteringState]
};

export interface ModelState {
  authorField: string;
  addressField: string;
  descriptionField: string;
  tags: Array<string>;
  majorNum: number;
}

export const initModelState: ModelState = {
  authorField: "",
  addressField: "",
  descriptionField: "",
  tags: [],
  majorNum: 0
};

export interface ModelTypeState {
  nameField: string;
  descriptionField: string;
}

export interface AlgoState {
  nameField: string;
  descriptionField: string;
  //tags: Array<string>,
}

export const initAlgoState = {
  nameField: "",
  descriptionField: ""
  // tags: [],
};

export interface SourceState {
  modelTypeName: string;
  parameters: string;
}

export const initSourceState: SourceState = {
  modelTypeName: "",
  parameters: ""
};

export interface ModelIdState {
  modelType: ModelTypeDef;
  majorNum: Optn<number>;
  minorNum: Optn<number>;
}

export interface DatasetState {
  name: string;
  description: string;
  url: string;
}

export const initDatasetState: DatasetState = {
  name: "",
  description: "",
  url: ""
};

export interface RegisterModelMatchState {
  modelType: Optn<ModelTypeDef>;
  algo: Optn<AlgoDef>;
  usage: Optn<UsageDef>;
  input: Optn<DataTypeDef>;
  output: Optn<DataTypeDef>;
  sourceModelType: Optn<ModelIdState>;
}

export const initRegisterModelMatchState: RegisterModelMatchState = {
  modelType: optNone(),
  algo: optNone(),
  usage: optNone(),
  input: optNone(),
  output: optNone(),
  sourceModelType: optNone()
};

export const initModelTypeState = {
  nameField: "",
  descriptionField: ""
};

export interface DataTypeState {
  nameField: string;
  descriptionField: string;
}

export const initDataTypeState = {
  nameField: "",
  descriptionField: ""
};

export interface UsageRenderState {
  nameField: string;
  descriptionField: string;
}

export const initUsageRenderState = {
  nameField: "",
  descriptionField: ""
};

export interface ModelMngDictState {
  modelDict: ModelDict;
  modelTypeDict: ModelTypeDict;
  algoDict: AlgoDict;
  usageDict: UsageDict;
  dataTypeDict: DataTypeDict;
  evalDict: EvalDict;
  effDict: EffDict;
  datasetDict: DatasetDict;
}

export const initModelMngDictState: ModelMngDictState = {
  modelDict: [],
  modelTypeDict: [],
  algoDict: [],
  usageDict: [],
  dataTypeDict: [],
  evalDict: [],
  effDict: [],
  datasetDict: []
};
