import { UsrID } from "./teamManage";
import { Option, None, Some, Result, Err, Ok } from "@usefultools/monads";
import { Optn, optSome, optNone } from "../tools";

export type IDHash = string;
export type Label = IDHash;
export type Item = IDHash;

export type ModelDict = Array<Model>;

export type ModelID = Label;

export interface Model {
  _id: ModelID;
  knowledge: ModelKnowledge;
}

export interface Source {
  previousModel: ModelID;
  params: string;
  dataset: DatasetLabel;
}

export interface ModelKnowledge {
  ownerID: OwnerID;
  authorID: AuthorID;
  description: string;
  address: string;
  tags: Array<string>;
  source: Source;
  intrinsics: Intrinsics;
  summary: ModelSummary;
}

export interface ModelSummary {
  eff: Efficiencies;
  eval: Evaluations;
}

export type DatasetLabel = Label;

export type DatasetDict = Array<DatasetDef>;

export interface DatasetDef {
  _id: DatasetLabel;
  knowledge: DatasetKnowledge;
}

export interface DatasetKnowledge {
  name: string;
  owner: DatasetLabel;
  description: string;
  url: string;
}

export interface Intrinsics {
  modelType: ModelTypeLabel;
}

export type OwnerID = UsrID;
export type AuthorID = Optn<UsrID>;

export type ModelTypeDict = Array<ModelTypeDef>;

export type ModelTypeLabel = Label;

export interface ModelTypeDef {
  _id: ModelTypeLabel;
  knowledge: ModelTypeKnowledge;
}

export interface ModelTypeKnowledge {
  ownerID: UsrID;
  name: string;
  description: string;
  algo: AlgoLabel;
  usage: UsageLabel;
  version: Versions;
}

export interface NewModelTypeKnowledge {
  ownerID: UsrID;
  name: string;
  description: string;
  algo: AlgoLabel;
  usage: UsageLabel;
}

export interface VersionPair {
  major: number;
  minor: number;
}

export interface Versions {
  newMajor: number;
  majorVersions: MajorVersion[];
}

export interface MajorVersion {
  num: number;
  newMinor: number;
  minorVersions: MinorVersion[];
}

export interface MinorVersion {
  num: number;
  modelID: ModelID;
}

export function findNewMinor(major: number, versions: Versions): number {
  // console.log("find new minor:");
  // console.log(major);
  // console.log(versions);
  let finding = versions.majorVersions.find(
    majorVersion => majorVersion.num === major
  );
  if (finding) {
    return finding.newMinor;
  } else {
    return 0;
  }
}

export function findVersion(
  md: Model,
  def: ModelTypeDef
): Result<VersionPair, string> {
  if (md.knowledge.intrinsics.modelType === def._id) {
    return def.knowledge.version.majorVersions.reduce(
      (acc: Result<VersionPair, string>, major: MajorVersion) => {
        return acc.match({
          ok: _ => acc,
          err: _ => {
            const findMinor = major.minorVersions.find(
              minor => minor.modelID === md._id
            );
            if (findMinor) {
              return Ok({
                major: major.num,
                minor: findMinor.num
              });
            } else {
              return acc;
            }
          }
        });
      },
      Err(
        `ModelType ID match but Model not found.  Model: ${md.knowledge.intrinsics.modelType}, ModelType: ${def._id} `
      )
    );
  } else {
    return Err<VersionPair, string>(
      `ModelType ID mismatch. Model: ${md.knowledge.intrinsics.modelType}, ModelType: ${def._id} `
    );
  }
}

export function majorVersions(mdt: ModelTypeDef): number[] {
  return mdt.knowledge.version.majorVersions
    .filter(mjv => !(minorVersions(mdt, mjv.num).length === 0))
    .map(mjv => mjv.num);
}

export function minorVersions(mdt: ModelTypeDef, major: number): number[] {
  // console.log('minorVersions mdt & major:');
  // console.log(mdt);
  // console.log(major);
  const mjrVer = mdt.knowledge.version.majorVersions[major];
  if (mjrVer) {
    return mjrVer.minorVersions.map(mnr => mnr.num);
  } else {
    return [];
  }
}

export type AlgoDict = Array<AlgoDef>;

export type AlgoLabel = Label;

export interface AlgoDef {
  _id: AlgoLabel;
  knowledge: AlgoKnowledge;
}

export interface AlgoKnowledge {
  name: string;
  ownerID: UsrID;
  description: string;
  // tags: Array<string>,
}

export type UsageDict = Array<UsageDef>;

export type UsageLabel = Label;

export interface UsageDef {
  _id: UsageLabel;
  knowledge: UsageKnowledge;
}

export interface UsageKnowledge {
  name: string;
  ownerID: UsrID;
  input: DataTypeLabel;
  output: DataTypeLabel;
  description: string;
}

export type DataTypeDict = Array<DataTypeDef>;

export type DataTypeLabel = Label;

export interface DataTypeDef {
  _id: DataTypeLabel;
  knowledge: DataTypeKnowledge;
}

export interface DataTypeKnowledge {
  name: string;
  ownerID: UsrID;
  description: string;
}

export type Evaluations = Array<EvalValue>;

export interface ItemValue {
  _id: Item;
  value: number;
}

export type EvalValue = ItemValue;

export type EvalDict = Array<EvalDef>;

export type EvalItem = Item;

export interface EvalDef {
  _id: EvalItem;
  knowledge: EvalKnowledge;
}

export interface EvalKnowledge {
  name: string;
  ownerID: UsrID;
  description: string;
  unit: string;
  dataset: DatasetLabel;
}

export type Efficiencies = Array<EffValue>;

export type EffValue = ItemValue;

export type EffDict = Array<EffDef>;

export type EffItem = Item;

export interface EffDef {
  _id: EffItem;
  knowledge: EffKnowledge;
}

export interface EffKnowledge {
  name: string;
  ownerID: UsrID;
  description: string;
  unit: string;
}

function findItemValue(item: Item, itemValues: ItemValue[]): Option<ItemValue> {
  let finding = itemValues.find(iv => iv._id === item);
  if (finding) {
    return Some(finding);
  } else {
    return None;
  }
}

export function findEvalValue(ei: EvalItem, md: Model): Option<EvalValue> {
  return findItemValue(ei, md.knowledge.summary.eval);
}

export function findEffValue(ei: EffItem, md: Model): Option<EffValue> {
  return findItemValue(ei, md.knowledge.summary.eff);
}

export interface MMDef {
  _id: IDHash;
  knowledge: {
    name: string;
  };
}

export function findDefByName<D extends MMDef>(
  name: string,
  dict: D[]
): Optn<D> {
  let finding = dict.find(def => def.knowledge.name === name);
  if (finding) {
    return optSome(finding);
  } else {
    return optNone();
  }
}

export function findDefByID<D extends MMDef>(id: IDHash, dict: D[]): Optn<D> {
  let finding = dict.find(def => def._id === id);
  if (finding) {
    return optSome(finding);
  } else {
    return optNone();
  }
}
