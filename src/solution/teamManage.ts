export type TMLv = 0 | 1 | 2 | 3 | 4;
export type MMLv = 0 | 1 | 2 | 3;
export type DMLv = 0 | 1 | 2 | 3;
export type PubLv = 0 | 1 | 2 | 3;
export type LvLabel = "Default" | "User" | "Maintainer" | "Admin" | "Super";

export function lvLabel(lv: TMLv | MMLv | DMLv): LvLabel {
  switch (lv) {
    case 0:
      return "Default";
    case 1:
      return "User";
    case 2:
      return "Maintainer";
    case 3:
      return "Admin";
    case 4:
      return "Super";
  }
}

export function tmToPub(tm: TMLv): PubLv {
  switch (tm) {
    case 0:
      return 0;
    case 1:
      return 1;
    case 2:
      return 2;
    case 3:
      return 3;
    case 4:
      return 3;
  }
}

export type UsrID = string;

export type Acnt = {
  userID: UsrID;
  userName: string;
  tmLevel: TMLv;
  mmLevel: MMLv;
  dmLevel: DMLv;
  isAlive: boolean;
};

export function newAcnt(
  userID: UsrID,
  userName: string,
  tmLevel: TMLv,
  mmLevel: MMLv,
  dmLevel: DMLv,
  isAlive: boolean
) {
  return { userID, userName, tmLevel, mmLevel, dmLevel, isAlive };
}

export const defaultAcnt = newAcnt("default@email", "DF", 0, 0, 0, false);

export type AcntRow = {
  userID: UsrID;
  userName: string;
  tmLevel: TMLv;
  mmLevel: MMLv;
  dmLevel: DMLv;
  editable: boolean;
};

function canEdit(usr: TMLv, target: TMLv): boolean {
  if (usr === 4) {
    return true;
  } else if (usr === 3 && target !== 4) {
    return true;
  } else if (usr === 2) {
    if (target === 4 || target === 3 || target === 2) {
      return false;
    } else {
      return true;
    }
  } else {
    return false;
  }
}

export function acntRow(usrTMLv: TMLv, acnt: Acnt): AcntRow {
  return {
    userID: acnt.userID,
    userName: acnt.userName,
    tmLevel: acnt.tmLevel,
    mmLevel: acnt.mmLevel,
    dmLevel: acnt.dmLevel,
    editable: canEdit(usrTMLv, acnt.tmLevel)
  };
}

function canViewAcnts(usrTMLv: TMLv): boolean {
  switch (usrTMLv) {
    case 0:
      return false;
    default:
      return true;
  }
}

export function makeAcntsTable(
  usrTMLv: TMLv,
  acntsList: Array<Acnt>
): Array<AcntRow> {
  if (canViewAcnts(usrTMLv)) {
    return acntsList.map(acnt => acntRow(usrTMLv, acnt));
  } else {
    return [];
  }
}

export function tmLvEditOptions(usr: TMLv, target: TMLv): Array<PubLv> {
  switch (usr) {
    case 0:
    case 1:
      return [];
    case 2:
      if (target >= 2) {
        return [];
      } else {
        return [0, 1, 2];
      }
    case 3:
    case 4:
      return [0, 1, 2, 3];
  }
}

export function otherLvEditOptions(
  editor: PubLv,
  origin: PubLv,
  tmChoice: PubLv
): Array<MMLv> {
  console.log("otherLvOptions:");
  console.log("editor:");
  console.log(editor);
  console.log("origin");
  console.log(origin);
  console.log("tmChoice");
  console.log(tmChoice);
  switch (editor) {
    case 0:
    case 1:
      return [origin];
    case 2:
      if (origin >= 2) {
        return [origin];
      } else {
        if (tmChoice >= 2) {
          if (tmChoice > 2) {
            console.log("otherLvEditOptions bug: editor = 2 & tmChoice > 2");
          }
          return [2];
        } else if (tmChoice === 0) {
          return [0];
        } else {
          return [0, 1, 2];
        }
      }
    case 3:
      switch (tmChoice) {
        case 0:
          return [0];
        case 1:
          return [0, 1, 2, 3];
        case 2:
          return [2, 3];
        case 3:
          return [3];
      }
    default:
      console.log("otherLvEditOptions unhandled case!");
      return [];
  }
}
