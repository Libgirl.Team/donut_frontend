import React from "react";
import ReactDOM from "react-dom";
import "./css/index.scss";
import CssBaseline from "@material-ui/core/CssBaseline";
import DonutApp from "./components/donutApp";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import { store, persistor } from "./store/index";
import { PersistGate } from "redux-persist/integration/react";
import ThemeProvider from "./ThemeProvider";

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ThemeProvider>
        <CssBaseline />
        <DonutApp />
      </ThemeProvider>
    </PersistGate>
  </Provider>,

  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
