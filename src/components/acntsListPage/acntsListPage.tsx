import React from "react";
import Divider from "@material-ui/core/Divider";
import { SearchZone } from "../common";
import { Acnt } from "../../solution/teamManage";
import { AcntsTable } from "./acntsTable";
import { ACNTS_LIST_PAGE } from "../../reducers/workingPage/acntsListReducer";

export interface AcntsListPageProps {
  type: typeof ACNTS_LIST_PAGE;
  userAcnt: Acnt;
  acntsList: Array<Acnt>;
  handleGotoEditAcnt: (acnt: Acnt) => void;
}

export function AcntsListPage(props: AcntsListPageProps) {
  return (
    <div className="acnts-list-page">
      <SearchZone />
      <Divider />
      {AcntsTable(
        props.acntsList,
        props.userAcnt.tmLevel,
        props.handleGotoEditAcnt
      )}
    </div>
  );
}
