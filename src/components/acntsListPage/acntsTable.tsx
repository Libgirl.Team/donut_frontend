import React from "react";
import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { Acnt, acntRow, TMLv } from "../../solution/teamManage";

export function AcntsTable(
  acnts: Array<Acnt>,
  usrTMLv: TMLv,
  handleGotoEditAcnt: (acnt: Acnt) => void
) {
  return (
    <Table className="acnts-table">
      <TableHead>
        <TableRow>
          <TableCell>User ID</TableCell>
          <TableCell align="right">User Name</TableCell>
          <TableCell align="right">TM Level</TableCell>
          <TableCell align="right">MM Level</TableCell>
          <TableCell align="right">DM Level</TableCell>
          <TableCell align="right">Action</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {acnts
          .filter(acnt => acnt.isAlive) // Should let Admin see the inactive accounts!!
          .filter(acnt => acnt.tmLevel !== 4)
          .map(acnt => {
            const row = acntRow(usrTMLv, acnt);
            return (
              <TableRow key={row.userID}>
                <TableCell component="th" scope="row">
                  {row.userID}
                </TableCell>
                <TableCell align="right">{row.userName}</TableCell>
                <TableCell align="right">{row.tmLevel}</TableCell>
                <TableCell align="right">{row.mmLevel}</TableCell>
                <TableCell align="right">{row.dmLevel}</TableCell>
                <TableCell align="right">
                  {row.editable ? EditAcntButton(acnt, handleGotoEditAcnt) : ""}
                </TableCell>
              </TableRow>
            );
          })}
      </TableBody>
    </Table>
  );
}

export function EditAcntButton(
  acnt: Acnt,
  handleGotoEditAcnt: (acnt: Acnt) => void
) {
  // const classes = useStyles();
  return (
    <Button
      variant="outlined"
      size="small"
      onClick={() => handleGotoEditAcnt(acnt)}
    >
      Edit
    </Button>
  );
}
