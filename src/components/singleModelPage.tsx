import React from "react";
import { SINGLE_MODEL_PAGE } from "../reducers/workingPage/singleModelPageReducer";
import {
  Button,
  Box,
  Grid,
  FormControl,
  Icon,
  TextField,
  Chip,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton
} from "@material-ui/core";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";

export interface SingleModelPageProps {
  type: typeof SINGLE_MODEL_PAGE;
}

export function ModelTitle(props: any) {
  return (
    <Box component="h2" mt={5}>
      {props.name}
    </Box>
  );
}

export function InfoCard(props: any) {
  return (
    <Box
      display="flex"
      p={2}
      className="model-card e-color"
      justifyContent="center"
      alignItems="center"
    >
      <Box mt={2}>
        <Box p={1}>
          <Icon style={{ fontSize: "5em" }}>appsRounded</Icon>
        </Box>
        <Box component="h3" m={1}>
          {props.name}
        </Box>
        <Box>
          <b>Version:</b> <span>{props.version}</span>
        </Box>
        <Box>
          <b>Owner:</b> <span>{props.owner}</span>
        </Box>
        <Box>
          <b>Autor:</b> <span>{props.autor}</span>
        </Box>
      </Box>
    </Box>
  );
}

export function SingleModelPage() {
  return (
    <Box className="models-list-page">
      <Grid container spacing={2}>
        <Grid item xs={8} style={{ textAlign: "left" }}>
          <Grid container style={{ textAlign: "center" }}>
            <Grid item xs={5}>
              <InfoCard
                name="model name"
                version="0.0"
                owner="owner＠owner.com"
                autor="autor@autor.com"
              />
            </Grid>
            <Grid item xs={7}>
              <Box p={2}>
                <h3 style={{ marginBottom: 0 }}>matrices</h3>
                <Table size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">m1</TableCell>
                      <TableCell align="center">m2</TableCell>
                      <TableCell align="center">m2</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <td className="m-color" align="center">
                        0.9
                      </td>
                      <td className="m-color" align="center">
                        0.8
                      </td>
                      <td className="m-color" align="center">
                        0.1
                      </td>
                    </TableRow>
                  </TableBody>
                </Table>
                <br />
                <h3 style={{ marginBottom: 0 }}>efficiencies</h3>
                <Table size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center">e1</TableCell>
                      <TableCell align="center">e2</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <td className="e-color" align="center">
                        512
                      </td>
                      <td className="e-color" align="center">
                        200
                      </td>
                    </TableRow>
                  </TableBody>
                </Table>
              </Box>
            </Grid>
          </Grid>

          <hr></hr>

          <ModelTitle name="Model Description" />
          <Box component="p">
            Chocolate recommendation resnet 18 trained by additional Singapore
            dataset
          </Box>

          <ModelTitle name="Address" />
          <Grid container spacing={1} alignItems="center">
            <Grid item xs={10}>
              <FormControl fullWidth>
                <TextField
                  value="gs://chocolate-recommendation/mn-2019-10-15.h5"
                  id="model_address"
                  aria-describedby="model address"
                  variant="outlined"
                />
              </FormControl>
            </Grid>
            <Grid item xs={2}>
              <IconButton
                color="primary"
                aria-label="copy address"
                component="span"
              >
                <FileCopyIcon />
              </IconButton>
            </Grid>
          </Grid>

          <ModelTitle name="Model Type" />
          <Box mb={1}>
            <Chip
              label="resnet18-chocolate"
              color="primary"
              clickable
              onClick={() => alert("go to definition page")}
            />
          </Box>

          <Box ml={5}>
            <h3>Algorithm</h3>
            <Box mb={1}>
              <Chip
                label="resnet18"
                color="primary"
                clickable
                onClick={() => alert("go to definition page")}
              />
            </Box>

            <h3>Usage</h3>
            <Box>
              <Chip
                label="chocolate_recommendation"
                color="primary"
                clickable
                onClick={() => alert("go to definition page")}
              />
            </Box>
          </Box>
          <Box p={2} mt={5} textAlign="center">
            <Button
              size="small"
              variant="outlined"
              color="secondary"
              endIcon={<Icon>delete</Icon>}
              onClick={() => alert("Unregistered!")}
            >
              Unregister this Model
            </Button>
          </Box>
        </Grid>
        <Grid item xs={4}>
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            p={2}
            style={{ background: "rgb(214, 214, 214)", height: "100%" }}
          >
            <Button
              color="primary"
              aria-label="deploy"
              endIcon={<Icon>arrow_right_alt</Icon>}
              onClick={() => alert("go to deploy")}
            >
              deploy this model
            </Button>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}
