import { Acnt, PubLv } from "../solution/teamManage";

export interface InputEvent {
  target: {
    value: string;
  };
}

export interface SelectLvEvent {
  target: {
    value: PubLv;
  };
}

export type BaseState = {};
export type BaseProps = {};

export type UserAcntProps = {
  userAcnt: Acnt;
};
