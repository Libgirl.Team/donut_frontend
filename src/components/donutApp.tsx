import React from "react";
import "./App.css";
import { connect } from "react-redux";
import { LoginPage } from "./loginPage/loginPage";
import { WorkingPage } from "./workingPage";
import { BaseState } from "./definitions";
import { mapDispatchToProps } from "../mapping/dispatchToProps/appMDTP";
import { workingPageProps } from "../props/workingPageProps";
import { DonutAppState } from "../reducers/index";

export type DonutAppPropMethods = ReturnType<typeof mapDispatchToProps>;
export type DonutAppProps = DonutAppPropMethods & {
  state: DonutAppState;
};

class DonutApp extends React.Component<DonutAppProps, BaseState> {
  render() {
    if (this.props.state.auth) {
      const props = workingPageProps(this.props);
      return <WorkingPage {...props} />;
    }
    return <LoginPage />;
  }
}

const mapStateToProps = (state: DonutAppState) => ({ state });

export default connect(mapStateToProps, mapDispatchToProps)(DonutApp);
