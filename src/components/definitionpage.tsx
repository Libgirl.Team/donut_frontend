import React from "react";
import { DEFINITION_PAGE } from "../reducers/workingPage/definitionReducer";
import { DefinBlock } from "./definBlock";
import {
  Button,
  Box,
  FormControl,
  Select,
  MenuItem,
  Divider,
  Paper,
  Icon,
  Table,
  Grid,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Link,
  Typography
} from "@material-ui/core";

export interface DefinitionPageProps {
  type: typeof DEFINITION_PAGE;
}

const handleGoback = () => {
  //window.history.back();
};

const handleEdit = () => {
  alert("switch to edit mode");
};

const handleSave = () => {
  alert("save something");
};

export function DefinitionPage() {
  return (
    <div className="definition-page">
      <div className="modeltype-definition-page">
        <Box p={1}>
          <Grid container spacing={1} justify="space-between">
            <Grid item>
              <Box>
                <Button
                  startIcon={<Icon>arrow_back</Icon>}
                  variant="contained"
                  onClick={handleGoback}
                >
                  Back
                </Button>
              </Box>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                startIcon={<Icon>edit</Icon>}
                color="primary"
                onClick={handleEdit}
                aria-label="edit"
              >
                Edit
              </Button>
            </Grid>
          </Grid>
        </Box>
        <Divider />
        <Box mb={5} mt={1}>
          {/* Model Type */}
          <Paper>
            <Box p={3}>
              <DefinBlock
                main_title="Model Type"
                label="resnet18-chocolate"
                owner="owner@owner.com"
                description="make recommendation (like or not) by resnet18 based on input image."
              />
            </Box>
          </Paper>
        </Box>
        <Box ml={5} pl={2} borderColor="primary.main" borderLeft={3}>
          <Box mb={5} mt={1}>
            {/* Algorithm */}
            <Paper>
              <Box p={3}>
                <DefinBlock
                  main_title="Algorithm"
                  label="resnet18"
                  owner="owner@owner.com"
                  hastag={true}
                  tags={<Link>#tag1</Link>}
                  description="CNN resnet18. reference paper: akgjawdlkgjadsgla."
                />
              </Box>
            </Paper>
          </Box>

          <Box mt={1}>
            {/* Usage */}
            <Paper>
              <Box p={3}>
                <DefinBlock
                  main_title="Usage"
                  label="chocolate-recommendation"
                  owner="owner@owner.com"
                  description="take input image and output whether the chocolate will be popular or not."
                />

                <Box ml={5} pl={2} borderColor="primary.main" borderLeft={3}>
                  <DefinBlock
                    main_title="Input"
                    label="img"
                    tags={<Link>#TAG</Link>}
                  />

                  <DefinBlock
                    main_title="output"
                    label="bool"
                    owner="Awner@owner.com"
                    description="Make recommendation (like or not) by resnet18 based on input imag."
                  />
                </Box>
              </Box>
            </Paper>
          </Box>
        </Box>

        <Box textAlign="center" p={2} mb={5}>
          <Button
            variant="contained"
            color="primary"
            size="large"
            startIcon={<Icon>save</Icon>}
            onClick={handleSave}
          >
            save edit
          </Button>
        </Box>
        <Divider />

        <Box mt={2} mb={3}>
          <Grid container>
            <Grid item>
              <Box component="h2">Major Version</Box>
            </Grid>
            <Grid item>
              <FormControl style={{ marginLeft: "10px" }}>
                <Select
                  labelId="label"
                  id="select"
                  value="1"
                  variant="outlined"
                >
                  <MenuItem value="1">1</MenuItem>
                  <MenuItem value="2">2</MenuItem>
                </Select>
              </FormControl>
            </Grid>
          </Grid>
        </Box>

        <Paper className="models-list">
          <Table style={{ wordBreak: "break-all" }} size="small">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell className="m-border" align="center" colSpan={3}>
                  matrices
                </TableCell>
                <TableCell className="e-border" align="center" colSpan={2}>
                  efficiencies
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>Version</TableCell>
                <TableCell align="center">precision</TableCell>
                <TableCell align="center">recall</TableCell>
                <TableCell align="center">f1</TableCell>
                <TableCell align="center">min memory(MB)</TableCell>
                <TableCell align="center">response time(ms)</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>1.0</TableCell>
                <TableCell className="m-color" align="center">
                  0.8
                </TableCell>
                <TableCell className="m-color" align="center">
                  0.8
                </TableCell>
                <TableCell className="m-color" align="center">
                  0.8
                </TableCell>
                <TableCell className="e-color" align="center">
                  512
                </TableCell>
                <TableCell className="e-color" align="center">
                  200
                </TableCell>
              </TableRow>

              <TableRow>
                <TableCell>1.1</TableCell>
                <TableCell align="center">
                  <Button
                    variant="contained"
                    size="small"
                    color="primary"
                    style={{ textTransform: "none" }}
                  >
                    Add
                  </Button>
                </TableCell>
                <TableCell align="center">
                  <Button
                    variant="contained"
                    size="small"
                    color="primary"
                    style={{ textTransform: "none" }}
                  >
                    Add
                  </Button>
                </TableCell>
                <TableCell align="center">
                  <Button
                    variant="contained"
                    size="small"
                    color="primary"
                    style={{ textTransform: "none" }}
                  >
                    Add
                  </Button>
                </TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>
      </div>
    </div>
  );
}
