import React from "react";

export enum LogoSize {
  Small = "small",
  Medium = "medium"
}

interface Props {
  size?: LogoSize;
  href?: string;
}

function logo(size: LogoSize = LogoSize.Small) {
  return (
    <img src="donut_logo.png" className={`site_logo site_logo--${size}`}></img>
  );
}

export default ({ size, href }: Props) => {
  if (href) {
    return <a href={href}>{logo(size)}</a>;
  }
  return logo(size);
};
