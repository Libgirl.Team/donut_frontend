import React from "react";
import {
  AcntsListPage,
  AcntsListPageProps
} from "./acntsListPage/acntsListPage";
import { ACNTS_LIST_PAGE } from "../reducers/workingPage/acntsListReducer";
import { MODELS_LIST_PAGE } from "../reducers/workingPage/modelsListReducer";
import { SINGLE_MODEL_PAGE } from "../reducers/workingPage/singleModelPageReducer";
import { UserInfoZone, UserInfoZoneProps } from "./userInfoZone";
import { ModelsListPage, ModelsListPageProps } from "./modelsListPage";
import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Divider from "@material-ui/core/Divider";
import { SingleModelPage, SingleModelPageProps } from "./singleModelPage";
import { RegisterModelPage, RegisterModelPageProps } from "./registerModelPage";
import { EditAcntPage, EditAcntPageProps } from "./editAcntPage";
import { EDIT_ACNT_PAGE } from "../reducers/workingPage/editAcntReducer";
import { DEFINITION_PAGE } from "../reducers/workingPage/definitionReducer";
import { DefinitionPage, DefinitionPageProps } from "./definitionpage";
import { REGISTER_MODEL_PAGE } from "../definitions/model";

export type WorkingPageProps = {
  userInfoZone: UserInfoZoneProps;
  variant: WorkingPagePropsVariant;
};

export type WorkingPagePropsVariant =
  | AcntsListPageProps
  | EditAcntPageProps
  | ModelsListPageProps
  | RegisterModelPageProps
  | DefinitionPageProps
  | SingleModelPageProps
  | UnhandledMarkProps;

export function WorkingPage(props: WorkingPageProps) {
  return (
    <div className="App">
      <Container maxWidth="md">
        <Box my={4}>
          {UserInfoZone(props.userInfoZone)}
          <Divider />
          {(() => {
            if (props.variant.type === ACNTS_LIST_PAGE) {
              return AcntsListPage(props.variant);
            } else if (props.variant.type === EDIT_ACNT_PAGE) {
              return EditAcntPage(props.variant);
            } else if (props.variant.type === MODELS_LIST_PAGE) {
              return ModelsListPage(props.variant);
            } else if (props.variant.type === SINGLE_MODEL_PAGE) {
              return SingleModelPage();
            } else if (props.variant.type === DEFINITION_PAGE) {
              return DefinitionPage();
            } else if (props.variant.type === REGISTER_MODEL_PAGE) {
              return RegisterModelPage(props.variant);
            } else {
              return UnhandleVariant();
            }
          })()}
          <Divider />
        </Box>
      </Container>
    </div>
  );
}

export const UNHANDLED_WP = "UNHANDLED_WP";

export interface UnhandledMarkProps {
  type: typeof UNHANDLED_WP;
}

export function UnhandleVariant() {
  return (
    <div>
      <p> unhandled working variant.</p>
    </div>
  );
}
