import React from "react";
import { MODELS_LIST_PAGE } from "../reducers/workingPage/modelsListReducer";
import {
  Button,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";
import {
  EvalDef,
  EffDef,
  findDefByID,
  findVersion,
  findEvalValue,
  findEffValue
} from "../solution/modelManage";
import { matchOptn } from "../tools";
import { ModelMngDictState } from "../definitions/model";

export interface ModelsListPageProps {
  type: typeof MODELS_LIST_PAGE;
  dict: ModelMngDictState;
}

interface GenRowFns {
  modelType: () => any;
  version: () => any;
  algo: () => any;
  usage: () => any;
  evals: (evalDefDict: EvalDef[]) => any[];
  effs: (effDefDict: EffDef[]) => any;
}

export function ModelsListPage(props: ModelsListPageProps) {
  const genRow = (fns: GenRowFns, key: string) => (
    <TableRow key={key}>
      {[fns.modelType(), fns.version(), fns.algo(), fns.usage()].concat(
        fns.evals(props.dict.evalDict),
        fns.effs(props.dict.effDict)
      )}
    </TableRow>
  );

  const header1 = genRow(
    {
      modelType: () => <TableCell key="mdt"></TableCell>,
      version: () => <TableCell key="ver"></TableCell>,
      algo: () => <TableCell key="algo"></TableCell>,
      usage: () => <TableCell key="usage"></TableCell>,
      evals: evalDefDict => [
        <TableCell
          className="m-border"
          align="center"
          key="eval"
          colSpan={evalDefDict.length}
        >
          Evaluations
        </TableCell>
      ],
      effs: effDefDict => [
        <TableCell
          className="e-border"
          align="center"
          key="eff"
          colSpan={effDefDict.length}
        >
          Efficiencies
        </TableCell>
      ]
    },
    "header1"
  );

  const header2 = genRow(
    {
      modelType: () => <TableCell key="mdt">Model Type</TableCell>,
      version: () => <TableCell key="ver">Version</TableCell>,
      algo: () => <TableCell key="algo">Algorithm</TableCell>,
      usage: () => <TableCell key="usage">Usage</TableCell>,
      evals: evalDefDict =>
        evalDefDict.map(def => (
          <TableCell key={def.knowledge.name}>{def.knowledge.name}</TableCell>
        )),
      effs: effDefDict =>
        effDefDict.map(def => (
          <TableCell key={def.knowledge.name}>{def.knowledge.name}</TableCell>
        ))
    },
    "header2"
  );

  const rowComponents = props.dict.modelDict.map(md => {
    return matchOptn(
      findDefByID(md.knowledge.intrinsics.modelType, props.dict.modelTypeDict),
      {
        some: foundModelType => {
          return genRow(
            {
              modelType: () => (
                <TableCell key="mdt">{foundModelType.knowledge.name}</TableCell>
              ),
              version: () => (
                <TableCell key="ver">
                  {findVersion(md, foundModelType).match({
                    ok: vp => `${vp.major}.${vp.minor}`,
                    err: err => err
                  })}
                </TableCell>
              ),
              algo: () => (
                <TableCell key="algo">
                  {matchOptn(
                    findDefByID(
                      foundModelType.knowledge.algo,
                      props.dict.algoDict
                    ),
                    {
                      some: algo => algo.knowledge.name,
                      none: () =>
                        `algo ${foundModelType.knowledge.algo} not found.`
                    }
                  )}
                </TableCell>
              ),
              usage: () => (
                <TableCell key="usage">
                  {matchOptn(
                    findDefByID(
                      foundModelType.knowledge.usage,
                      props.dict.usageDict
                    ),
                    {
                      some: usage => usage.knowledge.name,
                      none: () =>
                        `usage ${foundModelType.knowledge.usage} not found.`
                    }
                  )}
                </TableCell>
              ),

              evals: evalDefDict =>
                evalDefDict.map(def =>
                  findEvalValue(def._id, md).match({
                    some: evalValue => (
                      <TableCell
                        align="center"
                        key={def.knowledge.name + md._id}
                        className="m-color"
                      >
                        {evalValue.value}
                      </TableCell>
                    ),
                    none: (
                      <TableCell
                        align="center"
                        key={def.knowledge.name + md._id}
                        className="m-color"
                      >
                        <Button
                          variant="contained"
                          size="small"
                          color="primary"
                        >
                          add
                        </Button>
                      </TableCell>
                    )
                  })
                ),
              effs: effDefDict =>
                effDefDict.map(def => {
                  return findEffValue(def._id, md).match({
                    some: effValue => (
                      <TableCell
                        align="center"
                        key={def.knowledge.name + md._id}
                        className="e-color"
                      >
                        {effValue.value}
                      </TableCell>
                    ),
                    none: (
                      <TableCell
                        align="center"
                        key={def.knowledge.name + md._id}
                        className="e-color"
                      >
                        <Button
                          variant="contained"
                          size="small"
                          color="primary"
                        >
                          add
                        </Button>
                      </TableCell>
                    )
                  });
                })
            },
            md._id
          );
        },
        none: () => (
          <TableRow>{`modelType ${md.knowledge.intrinsics.modelType} not found.`}</TableRow>
        )
      }
    );
  });

  return (
    <div>
      <Paper className="models-list">
        <Table size="small">
          <TableHead>
            {header1}
            {header2}
          </TableHead>
          <TableBody>{rowComponents}</TableBody>
        </Table>
      </Paper>
    </div>
  );
}
