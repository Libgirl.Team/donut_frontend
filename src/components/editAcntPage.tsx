import React from "react";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Fab from "@material-ui/core/Fab";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { InputLabel, Typography, Button, Icon } from "@material-ui/core";
import { EDIT_ACNT_PAGE } from "../reducers/workingPage/editAcntReducer";
import {
  Acnt,
  lvLabel,
  tmLvEditOptions,
  MMLv,
  DMLv,
  PubLv,
  otherLvEditOptions,
  UsrID
} from "../solution/teamManage";

import { Paper, Grid } from "@material-ui/core";

export interface EditAcntPageProps {
  type: typeof EDIT_ACNT_PAGE;
  target: Acnt;
  usr: Acnt;
  tmChoice: PubLv;
  mmChoice: MMLv;
  dmChoice: DMLv;
  handleDeleteAcnt: (usrID: UsrID) => void;
  handleSelectTM: (tmChoice: PubLv, mmChoice: MMLv, dmChoice: DMLv) => void;
  handleSelectMM: (mmChoice: MMLv) => void;
  handleSelectDM: (dmChoice: DMLv) => void;
  handleCancel: () => void;
  handleSubmitEditAcnt: (
    target: UsrID,
    tmLv: PubLv,
    mmLv: MMLv,
    dmLv: DMLv
  ) => void;
}

export function EditAcntPage(props: EditAcntPageProps) {
  let tmOptions = tmLvEditOptions(props.usr.tmLevel, props.target.tmLevel);
  let mmOptions = otherLvEditOptions(
    props.usr.mmLevel,
    props.target.mmLevel,
    props.tmChoice
  );
  let dmOptions = otherLvEditOptions(
    props.usr.dmLevel,
    props.target.dmLevel,
    props.tmChoice
  );
  return (
    <Box textAlign="left" className="edit-acnt-page">
      <Box component="h2" mt={4}>
        Edit User Permission Levels
      </Box>
      <Box component="h3">
        <Button
          size="small"
          style={{ marginLeft: "10px" }}
          variant="outlined"
          color="secondary"
          startIcon={<Icon>delete</Icon>}
          onClick={() => props.handleDeleteAcnt(props.target.userID)}
        >
          Delete this account
        </Button>
      </Box>

      <Paper>
        <Box p={2}>
          <Box component="h3">Account Info</Box>
          <Typography component="p" key="user-id">
            <Box component="b">User ID: </Box>
            <Box component="span">{props.target.userID}</Box>
          </Typography>

          <Typography component="p" key="user-name">
            <Box component="b">User Name: </Box>
            <Box component="span">{props.target.userName}</Box>
          </Typography>
        </Box>
      </Paper>
      <Divider />

      <Box p={2} mt={2}>
        <Box m={0} component="h3">
          Permission Levels
        </Box>

        <Table className="target-acnt-info-table">
          <TableBody>
            <TableRow key="tm-lv-row">
              <TableCell>Team Management (TM)</TableCell>
              <TableCell>
                {LvSelect(props.tmChoice, tmOptions, event => {
                  props.handleSelectTM(
                    event.target.value as PubLv,
                    props.mmChoice,
                    props.dmChoice
                  );
                })}
              </TableCell>
              <TableCell>
                <em>{"original: " + lvLabel(props.target.tmLevel)}</em>
              </TableCell>
            </TableRow>
          </TableBody>

          <TableBody>
            <TableRow key="mm-lv-row">
              <TableCell> Model Management (MM)</TableCell>
              <TableCell>
                {LvSelect(props.mmChoice, mmOptions, evt =>
                  props.handleSelectMM(evt.target.value as MMLv)
                )}
              </TableCell>
              <TableCell>
                <em>{"original: " + lvLabel(props.target.mmLevel)}</em>
              </TableCell>
            </TableRow>
          </TableBody>

          <TableBody>
            <TableRow key="dm-lv-row">
              <TableCell>Deplotment Management (DM)</TableCell>
              <TableCell>
                {LvSelect(props.dmChoice, dmOptions, evt =>
                  props.handleSelectDM(evt.target.value as DMLv)
                )}
              </TableCell>
              <TableCell>
                <em>{"original: " + lvLabel(props.target.dmLevel)}</em>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Box>

      <Box textAlign="center" bgcolor="#eee" p={2}>
        <Grid container alignItems="center" justify="center" spacing={1}>
          <Grid item>
            <Button
              variant="outlined"
              color="default"
              aria-label="add"
              className="margin"
              onClick={() => {
                props.handleCancel();
              }}
            >
              Cancel
            </Button>
          </Grid>

          <Grid item>
            <Button
              variant="contained"
              color="primary"
              aria-label="add"
              className="margin"
              onClick={() => {
                props.handleSubmitEditAcnt(
                  props.target.userID,
                  props.tmChoice,
                  props.mmChoice,
                  props.dmChoice
                );
              }}
            >
              Edit
              <Icon>checked</Icon>
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}

function LvSelect(
  choice: PubLv,
  options: Array<PubLv>,
  handleChange: (event: React.ChangeEvent<{ value: unknown }>) => void
) {
  return (
    <FormControl variant="outlined" fullWidth>
      <Select value={choice} onChange={handleChange}>
        {options.map(lv => {
          return (
            <MenuItem value={lv} key={"lv" + lv}>
              {lvLabel(lv)}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
}
