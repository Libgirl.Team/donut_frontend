import React from "react";
import { lvLabel, Acnt } from "../solution/teamManage";
import MainMenu from "./navigation";
import Identify from "./userIdentify";
import { Button, Grid, Link, Box, Divider } from "@material-ui/core";
export interface UserInfoZoneProps {
  userAcnt: Acnt;
  handleLogout: () => void;
  handleGotoModelsList: () => void;
  handleGotoRegisterModel: () => void;
  handleGotoSingleModel: () => void;
  handleGotoAcntsList: () => void;
  handleGotoDefinition: () => void;
}

export function UserInfoZone(props: UserInfoZoneProps) {
  return (
    <Box>
      <Box mb={2}>
        <Grid container>
          <Grid item xs={6}>
            <Box className="logo">
              <Link href="#">
                <img
                  src="donut_logo.png"
                  style={{ height: "60px", textAlign: "left" }}
                ></img>
              </Link>
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Grid
              className="user-info"
              container
              spacing={1}
              alignItems="flex-start"
              justify="flex-end"
            >
              <Identify
                userName={props.userAcnt.userName}
                tmLv={lvLabel(props.userAcnt.tmLevel)}
                mmLv={lvLabel(props.userAcnt.mmLevel)}
                dmLv={lvLabel(props.userAcnt.dmLevel)}
                logout={() => props.handleLogout()}
              />
            </Grid>
          </Grid>
        </Grid>
      </Box>
      <Divider></Divider>

      <Box mt={2} mb={2} textAlign="right">
        <MainMenu></MainMenu>

        <Button
          className="to-md-list"
          onClick={() => props.handleGotoModelsList()}
        >
          Models List
        </Button>
        <Button className="to-endpoint-list">Endpoints List</Button>
        <Button
          className="register-md"
          onClick={() => props.handleGotoRegisterModel()}
        >
          Register Model
        </Button>
        <Button
          className="to-single-md-page"
          onClick={() => props.handleGotoSingleModel()}
        >
          Single Model
        </Button>
        <Button
          className="to-acnts-list"
          onClick={() => props.handleGotoAcntsList()}
        >
          Accounts List
        </Button>
        <Button
          className="to-definition"
          onClick={() => props.handleGotoDefinition()}
        >
          Definition
        </Button>
      </Box>
    </Box>
  );
}
