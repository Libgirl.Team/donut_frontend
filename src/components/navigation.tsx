import React from "react";
import {
  Toolbar,
  Button,
  Grid,
  Icon,
  Link,
  Box,
  Popover,
  Menu,
  MenuItem
} from "@material-ui/core";

let navs = [
  {
    main: "Team Management",
    sub: ["Account List"]
  },
  {
    main: "Model Management",
    sub: ["Model list", "Register Model", "Definition"]
  }
];

export default function MainMenu() {
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  let menu = navs.map(function(nav) {
    return (
      <Button aria-describedby={id} onClick={handleClick}>
        {nav.main}
      </Button>
    );
  });

  return (
    <Box textAlign="right">
      {menu}

      <Popover
        //id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
      >
        <MenuItem onClick={handleClose}>Models List</MenuItem>
        <MenuItem onClick={handleClose}>Register Model</MenuItem>
        <MenuItem onClick={handleClose}>Single Model</MenuItem>
        <MenuItem onClick={handleClose}>Accounts List</MenuItem>
        <MenuItem onClick={handleClose}>Definition</MenuItem>
      </Popover>
    </Box>
  );
}
