import React, { Dispatch } from "react";
import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";
import { handleGoogleLogin } from "../../actions/authPageActions";
import Logo from "../Logo";
import { useDispatch } from "react-redux";

const handleLogin = (dispatch: Dispatch<any>) => {
  dispatch(handleGoogleLogin(dispatch));
};

export function LoginPage() {
  const dispatch = useDispatch();

  return (
    <Card className="centered_card login">
      <Logo href="https://donut.libgirl.com/" />
      <div>
        <p>
          Please sign in to the application using your Google Cloud Platform
          account.
        </p>
        <Button
          onClick={() => handleLogin(dispatch)}
          variant="outlined"
          size="large"
          color="primary"
          style={{ marginBottom: ".75rem" }}
        >
          Login with GCP
        </Button>
        <Button color="secondary">I don't have a GCP account</Button>
      </div>
      <Button size="small">Back to homepage</Button>
    </Card>
  );
}
