import React from "react";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { REGISTER_MODEL_PAGE } from "../definitions/model";
import {
  ModelInfoZone,
  ModelInfoZoneProps
} from "./registerModelPage/modelInfoZone";
import {
  ModelTypeZone,
  ModelTypeZoneProps
} from "./registerModelPage/modelTypeZone";
import { SummaryZone, SummaryZoneProps } from "./registerModelPage/summaryZone";
import { SourceZone, SourceZoneProps } from "./registerModelPage/sourceZone";

export interface RegisterModelPageProps {
  type: typeof REGISTER_MODEL_PAGE;
  modelInfoZone: ModelInfoZoneProps;
  modelTypeZone: ModelTypeZoneProps;
  summaryZone: SummaryZoneProps;
  handleSubmitRegisterModel: () => void;
  sourceZone: SourceZoneProps;
}

export function RegisterModelPage(props: RegisterModelPageProps) {
  const modelInfoZone = ModelInfoZone(props.modelInfoZone);

  // const mtc = params.match;
  /* console.log('registerModelPage params.usageDict: ');
   * console.log(params.usageDict);*/

  const modelTypeZone = ModelTypeZone(props.modelTypeZone);

  return (
    <div>
      <Box p={2} style={{ backgroundColor: "#eeeeee", height: "100px" }}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => props.handleSubmitRegisterModel()}
          style={{ marginTop: "20px" }}
        >
          Register this model
        </Button>
      </Box>
      <Grid container spacing={3}>
        <Grid item xs={7}>
          {modelInfoZone}
          {SourceZone(props.sourceZone)}
          {modelTypeZone}
        </Grid>
        {SummaryZone(props.summaryZone)}
      </Grid>
    </div>
  );
}
