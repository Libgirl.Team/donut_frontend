import React from "react";
import {
  Tooltip,
  Typography,
  Avatar,
  Button,
  Grid,
  Icon,
  Link,
  Box,
  Chip
} from "@material-ui/core";

export default function Identify(props: any) {
  return (
    <Box>
      <Grid container spacing={1} alignItems="center" justify="flex-end">
        <Grid item>
          <Avatar
            src=""
            alt={props.userName}
            style={{
              margin: 5,
              width: 50,
              height: 50,
              border: "4px solid #eee"
            }}
          >
            {props.userName}
          </Avatar>
        </Grid>
        <Grid item>
          <h2>{props.userName}</h2>
        </Grid>
        <Grid item>
          <Box pl={2}>
            <Button
              size="small"
              variant="outlined"
              color="secondary"
              endIcon={<Icon>logout</Icon>}
              onClick={props.logout}
            >
              logout
            </Button>
          </Box>
        </Grid>
      </Grid>

      <Tooltip
        title={
          <React.Fragment>
            <Typography color="inherit">Team Management</Typography>
            {"Level:"} <b>{props.tmLv}</b>
          </React.Fragment>
        }
      >
        <Chip
          size="small"
          variant="outlined"
          color="primary"
          label={props.tmLv}
          avatar={<Avatar>T</Avatar>}
          style={{ margin: 3 }}
        />
      </Tooltip>
      <Tooltip
        title={
          <React.Fragment>
            <Typography color="inherit">Model Management</Typography>
            {"Level:"} <b>{props.mmLv}</b>
          </React.Fragment>
        }
      >
        <Chip
          size="small"
          variant="outlined"
          label={props.mmLv}
          avatar={<Avatar>M</Avatar>}
          style={{ margin: 3 }}
        />
      </Tooltip>

      <Tooltip
        title={
          <React.Fragment>
            <Typography color="inherit">Deplotment Management</Typography>
            {"Level:"} <b>{props.dmLv}</b>
          </React.Fragment>
        }
      >
        <Chip
          size="small"
          variant="outlined"
          label={props.dmLv}
          avatar={<Avatar>D</Avatar>}
          style={{ margin: 3 }}
        />
      </Tooltip>
    </Box>
  );
}
