import React from "react";
import {
  Button,
  Box,
  Typography,
  TextField,
  FormControl
} from "@material-ui/core";

export function EditInput(props: any) {
  return (
    <TextField
      fullWidth={props.fullWidth}
      id="outlined-helperText"
      label={props.label}
      defaultValue={props.value}
      margin="normal"
      variant="outlined"
    />
  );
}

export function TagBlock(props: any) {
  if (props.hastag) {
    return (
      <Box component="p">
        <Typography>
          <b>{props.tags}</b>
        </Typography>

        {/* <EditInput
                label="#Tag"
                defaultValue= {props.tags}
                fullWidth = {true}
                ></EditInput> */}
      </Box>
    );
  } else {
    return null;
  }
}

export function DefinBlock(props: any) {
  return (
    <Box textAlign="left" mb={1}>
      <Box component="h3" m={0} p={1}>
        {props.main_title}
      </Box>
      <Box>
        <Button
          style={{ textTransform: "none" }}
          variant="contained"
          id=""
          color="primary"
          size="small"
          aria-describedby="{props.label}"
        >
          {props.label}
        </Button>

        {/* <EditInput
                label="Label"
                value= {props.label}
                fullWidth = {false}
                ></EditInput> */}

        <TagBlock hastag={props.hastag} tags={props.tags} />
      </Box>

      <Box component="p" pl={2}>
        <Box component="span">owner_id : </Box>
        <Box component="span">{props.owner}</Box>
        {/* <EditInput
                label="Owner id"
                value= {props.owner}
                fullWidth ={true}
                ></EditInput> */}
      </Box>

      <Box component="p" pl={2}>
        <Box component="span">description : </Box>
        <Box component="span">{props.description}</Box>
        {/* <EditInput
                label="Description"
                value= {props.description}
                fullWidth ={true}
                ></EditInput> */}
      </Box>
    </Box>
  );
}

TagBlock.defaultProps = {
  tags: "no tag...",
  hastag: false
};

DefinBlock.defaultProps = {
  owner: "null",
  label: "null",
  description: "null"
};
