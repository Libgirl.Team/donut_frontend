import React from "react";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import { InputEvent } from "../definitions";

export function LabelDefinition(
  name: string,
  description: string,
  handleChangeName: (field: string) => void,
  handleChangeDescription: (field: string) => void
) {
  return (
    <div>
      <FormControl fullWidth>
        <TextField
          value={name}
          variant="outlined"
          margin="normal"
          label="Name"
          onChange={(evt: InputEvent) => handleChangeName(evt.target.value)}
        />
      </FormControl>
      <FormControl fullWidth>
        <TextField
          multiline={true}
          rows={3}
          margin="normal"
          label="Description"
          variant="outlined"
          value={description}
          onChange={(evt: InputEvent) =>
            handleChangeDescription(evt.target.value)
          }
        />
      </FormControl>
    </div>
  );
}
