import React from "react";
import Box from "@material-ui/core/Box";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";

export interface FillItemValueProps {
  name: string;
  value: string;
  handleChangeName: (nameField: string) => void;
  handleChangeValue: (valueField: string) => void;
}

export function FillItemValue(props: FillItemValueProps) {
  return (
    <Box
      key={`${props.name}_${props.value}`}
      pt={1}
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Box p={1}>
        <FormControl>
          <TextField
            value={props.name}
            onChange={evt => props.handleChangeName(evt.target.value)}
            variant="outlined"
            style={{ width: 100 }}
          />
        </FormControl>
      </Box>
      <Box p={1} style={{ fontSize: "2em" }}>
        :
      </Box>
      <Box p={1}>
        <FormControl>
          <TextField
            value={props.value}
            onChange={evt => props.handleChangeValue(evt.target.value)}
            variant="outlined"
            style={{ width: 100 }}
          />
        </FormControl>
      </Box>
    </Box>
  );
}
