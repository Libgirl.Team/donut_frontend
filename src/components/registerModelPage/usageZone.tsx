import React from "react";
import { Paper, Icon, Button, Box } from "@material-ui/core";
import {
  UsageRenderState,
  ModelMngDictState,
  DataTypeState
} from "../../definitions/model";
import { LabelDefinition } from "../common/labelDefinition";

export interface UsageZoneProps {
  usageDisplay: UsageRenderState;
  inputDisplay: DataTypeState;
  outputDisplay: DataTypeState;
  dict: ModelMngDictState;
  handleChangeUsageName: (field: string) => void;
  handleChangeUsageDescription: (field: string) => void;
  handleChangeInputName: (field: string) => void;
  handleChangeInputDescription: (field: string) => void;
  handleChangeOutputName: (field: string) => void;
  handleChangeOutputDescription: (field: string) => void;
}

export function UsageZone(props: UsageZoneProps) {
  return (
    <Paper style={{ padding: 10, marginBottom: 10 }}>
      <Box component="h2" m={0}>
        Usage
      </Box>
      {LabelDefinition(
        props.usageDisplay.nameField,
        props.usageDisplay.descriptionField,
        props.handleChangeUsageName,
        props.handleChangeUsageDescription
      )}
      <p>
        {"Usages: " +
          props.dict.usageDict.map(def => def.knowledge.name).join(", ") +
          "."}
      </p>
      <Box ml="4">
        <p>
          {"DataTypes: " +
            props.dict.dataTypeDict.map(def => def.knowledge.name).join(", ") +
            "."}
        </p>
        <Box p={2} mb={2} bgcolor="#eee">
          <Box component="h4" m={0}>
            Input type
          </Box>
          {LabelDefinition(
            props.inputDisplay.nameField,
            props.inputDisplay.descriptionField,
            props.handleChangeInputName,
            props.handleChangeInputDescription
          )}
        </Box>
        <Box p={2} bgcolor="#eee">
          <Box component="h4" m={0}>
            Output type
          </Box>
          {LabelDefinition(
            props.outputDisplay.nameField,
            props.outputDisplay.descriptionField,
            props.handleChangeOutputName,
            props.handleChangeOutputDescription
          )}
        </Box>
      </Box>
      <Box p={1} textAlign="right">
        <Button variant="outlined" color="default" style={{ margin: 8 }}>
          cancel
        </Button>
        <Button
          variant="contained"
          color="primary"
          endIcon={<Icon>check</Icon>}
          style={{ margin: 8 }}
        >
          comfirm
        </Button>
      </Box>
    </Paper>
  );
}
