import React from "react";
import {
  ModelTypeState,
  AlgoState,
  ModelMngDictState
} from "../../definitions/model";
import { InputEvent } from "../definitions";
import { ModelTypeDef } from "../../solution/modelManage";
import {
  Paper,
  FormControl,
  Grid,
  Icon,
  Button,
  Box,
  TextField,
  Select,
  MenuItem
} from "@material-ui/core";
import { Optn } from "../../tools";
import { UsageZone, UsageZoneProps } from "./usageZone";

export interface ModelTypeZoneProps {
  modelType: ModelTypeState;
  algo: AlgoState;
  usageZone: UsageZoneProps;
  dict: ModelMngDictState;
  matchModelType: Optn<ModelTypeDef>;
  majorVer: number;
  newMajor: number;
  newMinor: number;
  handleChangeModelTypeName: (field: string) => void;
  handleChangeModelTypeDescription: (field: string) => void;
  handleChangeAlgoName: (field: string) => void;
  handleChangeAlgoDesciption: (field: string) => void;
  handleSelectVersion: (major: number) => void;
}

export function ModelTypeZone(props: ModelTypeZoneProps) {
  /* const newMajorNum = matchOptn(props.matchModelType, {
     *     some: (def) => def.knowledge.version.newMajor,
     *     none: () => 0,
     * })

     * const newMinorNum = matchOptn(props.matchModelType, {
     *     some: (def) => findNewMinor(props.majorVer, def.knowledge.version),
     *     none: () => 0,
     * });*/

  /* console.log(`version#: ${newMajorNum}.${newMinorNum}`);
   * console.log(matchModelType);*/

  const VersionSelect = (
    <Select
      variant="outlined"
      defaultValue="2"
      placeholder="major"
      value={props.majorVer}
      onChange={(evt: React.ChangeEvent<{ value: unknown }>) => {
        props.handleSelectVersion(evt.target.value as number);
      }}
    >
      {[...Array(props.newMajor + 1).keys()].map(n => (
        <MenuItem value={n} key={n}>
          {n}
        </MenuItem>
      ))}
    </Select>
  );

  return (
    <Paper>
      <Box p={2} textAlign="left">
        <Box component="h2" m={0}>
          Model type
        </Box>

        <FormControl fullWidth>
          <TextField
            value={props.modelType.nameField}
            onChange={(evt: InputEvent) =>
              props.handleChangeModelTypeName(evt.target.value)
            }
            required
            variant="outlined"
            margin="normal"
            label="Name"
          />
          <p>
            {"ModelTypes: " +
              props.dict.modelTypeDict
                .map(def => def.knowledge.name)
                .join(", ") +
              "."}
          </p>
        </FormControl>

        <Box bgcolor="#eee" p={1} borderRadius="borderRadius">
          <Grid container alignItems="center" justify="flex-start">
            <Grid item>
              <Box fontSize="1em" p={1}>
                Version :
              </Box>
            </Grid>
            <Grid item>
              <FormControl fullWidth style={{ width: 60 }}>
                {VersionSelect}
              </FormControl>
            </Grid>
            <Grid item>
              <Box p={1} fontSize="1em">
                <Box component="span">.</Box>
                <Box component="span">{props.newMinor}</Box>
              </Box>
            </Grid>
          </Grid>
        </Box>

        <Box mb={2}>
          <FormControl fullWidth>
            <TextField
              multiline={true}
              rows={3}
              label="Description"
              variant="outlined"
              margin="normal"
              value={props.modelType.descriptionField}
              onChange={(evt: InputEvent) =>
                props.handleChangeModelTypeDescription(evt.target.value)
              }
            />
          </FormControl>
        </Box>
        <hr></hr>

        <Box ml={3} mt={2}>
          <Box bgcolor="#eee" p={2} borderRadius="borderRadius">
            <Box component="h2" m={0}>
              Algorithm Name
            </Box>
            <FormControl fullWidth>
              <TextField
                value={props.algo.nameField}
                onChange={(evt: InputEvent) =>
                  props.handleChangeAlgoName(evt.target.value)
                }
                required
                label="Name"
                margin="normal"
                variant="outlined"
              />
              <p>
                {"Algorithms: " +
                  props.dict.algoDict
                    .map(def => def.knowledge.name)
                    .join(", ") +
                  "."}
              </p>
            </FormControl>

            <FormControl fullWidth>
              <TextField
                multiline={true}
                rows={3}
                label="Description"
                variant="outlined"
                margin="normal"
                value={props.algo.descriptionField}
                onChange={(evt: InputEvent) =>
                  props.handleChangeAlgoDesciption(evt.target.value)
                }
              />
            </FormControl>
          </Box>

          <Box component="p" color="secondary.main" mt={2} textAlign="center">
            Usage is empty
          </Box>
          <Box mb={2} textAlign="center">
            <Button
              variant="outlined"
              size="small"
              endIcon={<Icon>arrow_forward</Icon>}
            >
              fill up Usage
            </Button>
          </Box>
          {UsageZone(props.usageZone)}
        </Box>
      </Box>
    </Paper>
  );
}
