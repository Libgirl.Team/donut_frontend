import React from "react";
import Grid from "@material-ui/core/Grid";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import Box from "@material-ui/core/Box";
import FormLabel from "@material-ui/core/FormLabel";
import { ItemRegisteringState } from "../../definitions/model";
import { FillItemValue, FillItemValueProps } from "../common/fillItemValue";

export interface SummaryZoneProps {
  isShowingEval: boolean;
  isShowingEff: boolean;
  handleClickShowingEval: () => void;
  handleClickShowingEff: () => void;
  evalTable: SummaryTableProps;
  effTable: SummaryTableProps;
}

export function SummaryZone(props: SummaryZoneProps) {
  return (
    <Grid item xs={5}>
      <Box component="h2" mb={0} borderBottom={1} textAlign="left">
        Summary
      </Box>
      <Box mb={2} p={1} bgcolor="#eee">
        <FormControlLabel
          control={
            <Checkbox
              checked={props.isShowingEval}
              onChange={props.handleClickShowingEval}
              value="checked"
              color="primary"
            />
          }
          label="Matrices"
        />
        <FormControlLabel
          control={
            <Checkbox
              checked={props.isShowingEff}
              onChange={props.handleClickShowingEff}
              value="checked"
              color="primary"
            />
          }
          label="Efficiencies"
        />
      </Box>

      <Paper style={{ marginBottom: 10 }}>
        {props.isShowingEval ? SummaryTable(props.evalTable) : null}
        {props.isShowingEval && props.isShowingEff ? <hr></hr> : null}
        {props.isShowingEff ? SummaryTable(props.effTable) : null}
      </Paper>
    </Grid>
  );
}

export interface SummaryTableProps {
  title: string;
  rows: ItemRegisteringState[];
  addRow: () => void;
  handleEditlName: (n: number, nameField: string) => void;
  handleEditValue: (n: number, valueField: string) => void;
}

function SummaryTable(props: SummaryTableProps) {
  const rows = props.rows.map((state, index) => {
    const fillProps: FillItemValueProps = {
      name: state.nameField,
      value: state.valueField,
      handleChangeName: nameField => props.handleEditlName(index, nameField),
      handleChangeValue: valueField => props.handleEditValue(index, valueField)
    };
    return FillItemValue(fillProps);
  });
  return (
    <Box p={2}>
      <FormLabel component="legend">{props.title}</FormLabel>
      {rows}
      <Box p={2}>
        <Button
          color="default"
          size="small"
          variant="outlined"
          startIcon={<Icon color="primary">add_circle</Icon>}
          onClick={props.addRow}
        >
          Add row
        </Button>
      </Box>
    </Box>
  );
}
