import React from "react";
import { InputEvent } from "../definitions";
import { Box, FormControl, TextField } from "@material-ui/core";

export interface ModelInfoZoneProps {
  authorIdField: string;
  addressField: string;
  modelDescription: string;
  handleChangeAurhorField: (field: string) => void;
  handleChangeAddressField: (field: string) => void;
  handleChangeModelDescription: (field: string) => void;
}

export function ModelInfoZone(props: ModelInfoZoneProps) {
  return (
    <Box>
      <Box component="h2" mb={0} borderBottom={1} textAlign="left">
        Model info
      </Box>
      <Box mb={2}>
        <FormControl fullWidth>
          <TextField
            id="author_id"
            required
            variant="outlined"
            value={props.authorIdField}
            label="Author ID"
            margin="normal"
            onChange={(evt: InputEvent) =>
              props.handleChangeAurhorField(evt.target.value)
            }
          />
        </FormControl>

        <FormControl fullWidth>
          <TextField
            value={props.addressField}
            variant="outlined"
            onChange={(evt: InputEvent) =>
              props.handleChangeAddressField(evt.target.value)
            }
            required
            margin="normal"
            label="Model Address"
          />
        </FormControl>

        <FormControl fullWidth>
          <TextField
            multiline={true}
            rows={3}
            label="Description"
            variant="outlined"
            value={props.modelDescription}
            margin="normal"
            onChange={(evt: InputEvent) =>
              props.handleChangeModelDescription(evt.target.value)
            }
          />
        </FormControl>
      </Box>
    </Box>
  );
}
