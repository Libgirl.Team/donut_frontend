import React from "react";
import {
  Paper,
  FormControl,
  Grid,
  Icon,
  Button,
  Box,
  TextField,
  Select,
  MenuItem
} from "@material-ui/core";
import { Optn, matchOptn } from "../../tools";

export interface SourceZoneProps {
  modelType: string;
  handleChangeModelType: (field: string) => void;
  parameters: string;
  handleChangeParameters: (field: string) => void;
  datasetName: string;
  handleChangeDatasetname: (field: string) => void;
  datasetDescription: string;
  handleChangeDatasetDescription: (field: string) => void;
  datasetUrl: string;
  handleChangeDatasetUrl: (field: string) => void;
  versionsSelect: Optn<VersionSelectPairProps>;
}

export function SourceZone(props: SourceZoneProps) {
  return (
    <Paper>
      <Box p={2} textAlign="left">
        <Box component="h2" m={0}>
          Source
        </Box>

        <Box component="h3" m={0}>
          Previous Model
        </Box>
        <FormControl fullWidth>
          <TextField
            required
            variant="outlined"
            margin="normal"
            label="Model Type"
            value={props.modelType}
            onChange={evt => props.handleChangeModelType(evt.target.value)}
          />
        </FormControl>

        {matchOptn(props.versionsSelect, {
          some: vp => VersionSelectPair(vp),
          none: () => null
        })}

        <Box mb={2}>
          <FormControl fullWidth>
            <TextField
              multiline={true}
              rows={3}
              label="Parameters"
              variant="outlined"
              margin="normal"
              value={props.parameters}
              onChange={evt => props.handleChangeParameters(evt.target.value)}
            />
          </FormControl>
        </Box>

        <Box bgcolor="#eee" p={2} borderRadius="borderRadius">
          <Box component="h2" m={0}>
            Dataset
          </Box>
          <FormControl fullWidth>
            <TextField
              required
              label="Name"
              margin="normal"
              variant="outlined"
              value={props.datasetName}
              onChange={evt => props.handleChangeDatasetname(evt.target.value)}
            />
          </FormControl>

          <FormControl fullWidth>
            <TextField
              multiline={true}
              rows={3}
              label="Description"
              variant="outlined"
              margin="normal"
              value={props.datasetDescription}
              onChange={evt =>
                props.handleChangeDatasetDescription(evt.target.value)
              }
            />
          </FormControl>

          <FormControl fullWidth>
            <TextField
              required
              label="url"
              margin="normal"
              variant="outlined"
              value={props.datasetUrl}
              onChange={evt => props.handleChangeDatasetUrl(evt.target.value)}
            />
          </FormControl>
        </Box>
      </Box>
    </Paper>
  );
}

export interface VersionSelectPairProps {
  major: Optn<VerChoice>;
  minor: Optn<VerChoice>;
  handleSelectMajor: (n: number) => void;
  handleSelectMinor: (n: number) => void;
}

export interface VerChoice {
  choice: Optn<number>;
  options: number[];
}

function OptnSelect(opt: Optn<VerChoice>, handleSelect: (n: number) => void) {
  if (opt.isSome && opt.v.choice.isSome && !(opt.v.options.length === 0)) {
    return (
      <Select
        variant="outlined"
        defaultValue="2"
        placeholder="major"
        value={opt.v.choice.v}
        onChange={evt => handleSelect((evt.target.value as unknown) as number)}
      >
        {opt.v.options.map(n => (
          <MenuItem value={n} key={n}>
            {n}
          </MenuItem>
        ))}
      </Select>
    );
  } else {
    return (
      <Grid item>
        <Box p={1} fontSize="1em">
          <Box component="span">NA</Box>
        </Box>
      </Grid>
    );
  }
}

function VersionSelectPair(props: VersionSelectPairProps) {
  return (
    <Grid container alignItems="center" justify="flex-start">
      {OptnSelect(props.major, props.handleSelectMajor)}
      <Grid item>
        <Box p={1} fontSize="1em">
          <Box component="span">.</Box>
        </Box>
      </Grid>
      {OptnSelect(props.minor, props.handleSelectMinor)}
    </Grid>
  );
}
