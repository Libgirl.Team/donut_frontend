import React from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { useStyles } from "./styles";

export function SearchZone() {
  const classes = useStyles();
  return (
    <div className="search-zone">
      <Box height="50" className={classes.searchZoneBox}>
        <TextField
          className={classes.textField}
          id="outlined-bare"
          defaultValue="search"
          margin="dense"
          variant="outlined"
          inputProps={{ "aria-label": "bare" }}
        />
        <Button variant="contained" size="small" className={classes.margin}>
          Search
        </Button>
      </Box>
    </div>
  );
}
