import hardSet from "redux-persist/lib/stateReconciler/hardSet";
import { createStore, applyMiddleware, AnyAction } from "redux";
import Thunk from "redux-thunk";
import donutApp, { DonutAppState } from "../reducers/index";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

// w/ persistor
const persistConfig = {
  key: "root",
  storage,
  stateReconciler: hardSet
};

const persistedReducer = persistReducer<DonutAppState, AnyAction>(
  persistConfig,
  donutApp
);
export const store = createStore(persistedReducer, applyMiddleware(Thunk));
export const persistor = persistStore(store);

// no persistor
//export const store = createStore(donutApp, applyMiddleware(Thunk));
